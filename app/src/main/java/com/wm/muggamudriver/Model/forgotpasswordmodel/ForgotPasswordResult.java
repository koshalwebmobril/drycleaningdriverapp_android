package com.wm.muggamudriver.Model.forgotpasswordmodel;

import com.google.gson.annotations.SerializedName;

public class ForgotPasswordResult{

	@SerializedName("otp")
	private int otp;

	public int getOtp(){
		return otp;
	}
}