package com.wm.muggamudriver.Model.popupdatamodel;

import com.google.gson.annotations.SerializedName;

public class PopUpDataResponse{

	@SerializedName("code")
	private int code;

	@SerializedName("popupDetails")
	private PopupDetails popupDetails;

	@SerializedName("error")
	private boolean error;

	@SerializedName("message")
	private String message;

	public int getCode(){
		return code;
	}

	public PopupDetails getPopupDetails(){
		return popupDetails;
	}

	public boolean isError(){
		return error;
	}

	public String getMessage(){
		return message;
	}
}