package com.wm.muggamudriver.Model.updateprofilemodel;

import com.google.gson.annotations.SerializedName;

public class UpdateProfileResponse{

	@SerializedName("driverInfo")
	private UpdateProfileModel updateProfileModel;

	@SerializedName("code")
	private int code;

	@SerializedName("error")
	private boolean error;

	@SerializedName("message")
	private String message;

	public UpdateProfileModel getUpdateProfileModel(){
		return updateProfileModel;
	}

	public int getCode(){
		return code;
	}

	public boolean isError(){
		return error;
	}

	public String getMessage(){
		return message;
	}
}