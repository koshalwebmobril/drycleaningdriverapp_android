package com.wm.muggamudriver.Model.updateorderstatusmodel;

import com.google.gson.annotations.SerializedName;

public class UpdateOrderStatusResponse {

	@SerializedName("code")
	private int code;

	@SerializedName("error")
	private boolean error;

	@SerializedName("message")
	private String message;

	@SerializedName("booking_status")
	private int booking_status;

	public int getCode(){
		return code;
	}

	public int getBooking_status(){
		return booking_status;
	}

	public boolean isError(){
		return error;
	}

	public String getMessage(){
		return message;
	}
}