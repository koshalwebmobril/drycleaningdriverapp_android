package com.wm.muggamudriver.Model.acceptbookingmodel;

import com.google.gson.annotations.SerializedName;

public class AcceptBookingResponse {

	@SerializedName("code")
	private int code;

	@SerializedName("error")
	private boolean error;

	@SerializedName("message")
	private String message;

	public int getCode(){
		return code;
	}

	public boolean isError(){
		return error;
	}

	public String getMessage(){
		return message;
	}
}