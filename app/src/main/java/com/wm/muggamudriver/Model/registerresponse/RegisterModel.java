package com.wm.muggamudriver.Model.registerresponse;

import com.google.gson.annotations.SerializedName;

public class RegisterModel {

	@SerializedName("name")
	private String name;

	@SerializedName("mobile")
	private String mobile;

	@SerializedName("id")
	private int id;

	@SerializedName("type")
	private String type;

	@SerializedName("email")
	private String email;

	public String getName(){
		return name;
	}

	public String getMobile(){
		return mobile;
	}

	public int getId(){
		return id;
	}

	public String getType(){
		return type;
	}

	public String getEmail(){
		return email;
	}
}