package com.wm.muggamudriver.Model.allordermodel;

import com.google.gson.annotations.SerializedName;

public class AllOrderModel {

	@SerializedName("pickup_location")
	private String pickupLocation;

	@SerializedName("user_name")
	private String userName;

	@SerializedName("pickup_driver_id")
	private Object pickupDriverId;

	@SerializedName("is_picked_delivered")
	private String is_picked_delivered;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("booking_status")
	private int bookingStatus;


	@SerializedName("is_pickup_driver_delivered")
	private int is_pickup_driver_delivered;

	@SerializedName("is_delivery_driver_delivered")
	private int is_delivery_driver_delivered;

	@SerializedName("delivery_time")
	private String deliveryTime;

	@SerializedName("drop_location")
	private String dropLocation;

	@SerializedName("user_longitude")
	private String userLongitude;

	@SerializedName("user_latitude")
	private String userLatitude;

	@SerializedName("pickup_date")
	private String pickupDate;

	@SerializedName("delivery_date")
	private String deliveryDate;

	@SerializedName("user_profile_image")
	private Object userProfileImage;

	@SerializedName("user_id")
	private int userId;

	@SerializedName("user_mobile")
	private String userMobile;

	@SerializedName("is_provider_accepted")
	private int isProviderAccepted;

	@SerializedName("booked_at")
	private String bookedAt;

	@SerializedName("provider_id")
	private int providerId;

	@SerializedName("id")
	private int id;

	@SerializedName("pickup_time")
	private String pickupTime;

	@SerializedName("order_id")
	private String orderId;

	@SerializedName("delivery_driver_id")
	private Object deliveryDriverId;

	@SerializedName("provider_longitude")
	private String providerLongitude;

	@SerializedName("provider_latitude")
	private String providerLatitude;

	@SerializedName("is_rejected")
	private String is_rejected;

	@SerializedName("service_name")
	private String servicename;

	public String getPickupLocation(){
		return pickupLocation;
	}

	public String getServicename(){
		return servicename;
	}

	public String getIs_picked_delivered(){
		return is_picked_delivered;
	}

	public String getOrderRejected(){
		return is_rejected;
	}

	public String getUserName(){
		return userName;
	}

	public Object getPickupDriverId(){
		return pickupDriverId;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public int getBookingStatus(){
		return bookingStatus;
	}

	public int getIs_pickup_driver_delivered(){
		return is_pickup_driver_delivered;
	}

	public int is_delivery_driver_delivered(){
		return is_delivery_driver_delivered;
	}

	public String getDeliveryTime(){
		return deliveryTime;
	}

	public String getDropLocation(){
		return dropLocation;
	}

	public String getUserLongitude(){
		return userLongitude;
	}

	public String getUserLatitude(){
		return userLatitude;
	}

	public String getPickupDate(){
		return pickupDate;
	}

	public String getDeliveryDate(){
		return deliveryDate;
	}

	public Object getUserProfileImage(){
		return userProfileImage;
	}

	public int getUserId(){
		return userId;
	}

	public String getUserMobile(){
		return userMobile;
	}

	public int getIsProviderAccepted(){
		return isProviderAccepted;
	}

	public String getBookedAt(){
		return bookedAt;
	}

	public int getProviderId(){
		return providerId;
	}

	public int getId(){
		return id;
	}

	public String getPickupTime(){
		return pickupTime;
	}

	public String getOrderId(){
		return orderId;
	}

	public Object getDeliveryDriverId(){
		return deliveryDriverId;
	}

	public String getProviderLongitude(){
		return providerLongitude;
	}

	public String getProviderLatitude(){
		return providerLatitude;
	}
}