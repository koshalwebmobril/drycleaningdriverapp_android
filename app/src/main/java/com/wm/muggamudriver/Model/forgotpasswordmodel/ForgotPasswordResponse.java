package com.wm.muggamudriver.Model.forgotpasswordmodel;

import com.google.gson.annotations.SerializedName;

public class ForgotPasswordResponse{

	@SerializedName("forgotPasswordResult")
	private ForgotPasswordResult forgotPasswordResult;

	@SerializedName("code")
	private int code;

	@SerializedName("error")
	private boolean error;

	@SerializedName("message")
	private String message;

	public ForgotPasswordResult getForgotPasswordResult(){
		return forgotPasswordResult;
	}

	public int getCode(){
		return code;
	}

	public boolean isError(){
		return error;
	}

	public String getMessage(){
		return message;
	}
}