package com.wm.muggamudriver.Model.popupdatamodel;

import com.google.gson.annotations.SerializedName;

public class PopupDetails{

	@SerializedName("pickup_date")
	private String pickupDate;

	@SerializedName("delivery_date")
	private String deliveryDate;

	@SerializedName("pickup_location")
	private String pickupLocation;

	@SerializedName("id")
	private int id;

	@SerializedName("pickup_time")
	private String pickupTime;

	@SerializedName("delivery_time")
	private String deliveryTime;

	@SerializedName("drop_location")
	private String dropLocation;

	@SerializedName("user_name")
	private String user_name;

	@SerializedName("user_profile_image")
	private String user_profile_image;

	public String getPickupDate(){
		return pickupDate;
	}

	public String getDeliveryDate(){
		return deliveryDate;
	}
	public String getUsername(){
		return user_name;
	}
	public String getUser_Profile(){
		return user_profile_image;
	}




	public String getPickupLocation(){
		return pickupLocation;
	}

	public int getId(){
		return id;
	}

	public String getPickupTime(){
		return pickupTime;
	}

	public String getDeliveryTime(){
		return deliveryTime;
	}

	public String getDropLocation(){
		return dropLocation;
	}
}