package com.wm.muggamudriver.Model.registerresponse;

import com.google.gson.annotations.SerializedName;

public class RegisterResponse{

	@SerializedName("signupResult")
	private RegisterModel registerModel;

	@SerializedName("code")
	private int code;

	@SerializedName("error")
	private boolean error;

	@SerializedName("message")
	private String message;

	public RegisterModel getRegisterModel(){
		return registerModel;
	}

	public int getCode(){
		return code;
	}

	public boolean isError(){
		return error;
	}

	public String getMessage(){
		return message;
	}
}