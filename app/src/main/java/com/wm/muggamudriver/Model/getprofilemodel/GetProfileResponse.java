package com.wm.muggamudriver.Model.getprofilemodel;

import com.google.gson.annotations.SerializedName;

public class GetProfileResponse{

	@SerializedName("driverInfo")
	private GetProfileModel getProfileModel;

	@SerializedName("code")
	private int code;

	@SerializedName("error")
	private boolean error;

	public GetProfileModel getGetProfileModel(){
		return getProfileModel;
	}

	public int getCode(){
		return code;
	}

	public boolean isError(){
		return error;
	}
}