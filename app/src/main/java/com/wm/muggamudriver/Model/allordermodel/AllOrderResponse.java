package com.wm.muggamudriver.Model.allordermodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class AllOrderResponse {

	@SerializedName("code")
	private int code;

	@SerializedName("error")
	private boolean error;

	@SerializedName("message")
	private String message;

	@SerializedName("bookings")
	private List<AllOrderModel> bookings;

	public int getCode(){
		return code;
	}

	public boolean isError(){
		return error;
	}

	public String getMessage(){
		return message;
	}

	public List<AllOrderModel> getBookings(){
		return bookings;
	}
}