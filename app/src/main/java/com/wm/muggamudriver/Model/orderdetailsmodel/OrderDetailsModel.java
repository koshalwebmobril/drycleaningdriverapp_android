package com.wm.muggamudriver.Model.orderdetailsmodel;

import com.google.gson.annotations.SerializedName;

public class OrderDetailsModel {

	@SerializedName("is_pickup_driver_delivered")
	private int is_pickup_driver_delivered;

	@SerializedName("is_delivery_driver_delivered")
	private int is_delivery_driver_delivered;

	@SerializedName("pickup_location")
	private String pickupLocation;

	@SerializedName("user_image")
	private String userImage;

	@SerializedName("user_name")
	private String userName;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("booking_status")
	private int bookingStatus;

	@SerializedName("delivery_time")
	private String deliveryTime;

	@SerializedName("drop_location")
	private String dropLocation;

	@SerializedName("user_longitude")
	private String userLongitude;

	@SerializedName("user_latitude")
	private String userLatitude;

	@SerializedName("delivery_fee")
	private double deliveryFee;

	@SerializedName("pickup_date")
	private String pickupDate;

	@SerializedName("delivery_date")
	private String deliveryDate;

	@SerializedName("user_id")
	private int userId;

	@SerializedName("total_amount")
	private double totalAmount;

	@SerializedName("user_mobile")
	private String userMobile;

	@SerializedName("is_provider_accepted")
	private int isProviderAccepted;

	@SerializedName("provider_id")
	private int providerId;

	@SerializedName("id")
	private int id;

	@SerializedName("pickup_time")
	private String pickupTime;

	@SerializedName("order_id")
	private String orderId;

	@SerializedName("provider_longitude")
	private String providerLongitude;

	@SerializedName("provider_latitude")
	private String providerLatitude;

	public int getIs_pickup_driver_delivered(){
		return is_pickup_driver_delivered;
	}

	public int is_delivery_driver_delivered(){
		return is_delivery_driver_delivered;
	}



	public String getPickupLocation(){
		return pickupLocation;
	}

	public String getUserImage(){
		return userImage;
	}

	public String getUserName(){
		return userName;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public int getBookingStatus(){
		return bookingStatus;
	}

	public String getDeliveryTime(){
		return deliveryTime;
	}

	public String getDropLocation(){
		return dropLocation;
	}

	public String getUserLongitude(){
		return userLongitude;
	}

	public String getUserLatitude(){
		return userLatitude;
	}

	public double getDeliveryFee(){
		return deliveryFee;
	}

	public String getPickupDate(){
		return pickupDate;
	}

	public String getDeliveryDate(){
		return deliveryDate;
	}

	public int getUserId(){
		return userId;
	}

	public double getTotalAmount(){
		return totalAmount;
	}

	public String getUserMobile(){
		return userMobile;
	}

	public int getIsProviderAccepted(){
		return isProviderAccepted;
	}

	public int getProviderId(){
		return providerId;
	}

	public int getId(){
		return id;
	}

	public String getPickupTime(){
		return pickupTime;
	}

	public String getOrderId(){
		return orderId;
	}

	public String getProviderLongitude(){
		return providerLongitude;
	}

	public String getProviderLatitude(){
		return providerLatitude;
	}
}