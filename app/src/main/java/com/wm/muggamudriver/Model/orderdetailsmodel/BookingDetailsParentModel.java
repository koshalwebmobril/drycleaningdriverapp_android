package com.wm.muggamudriver.Model.orderdetailsmodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class BookingDetailsParentModel {

	@SerializedName("service_name")
	private String serviceName;

	@SerializedName("id")
	private int id;

	@SerializedName("items")
	private List<BookingDetailsChildModel> items;

	public String getServiceName(){
		return serviceName;
	}

	public int getId(){
		return id;
	}

	public List<BookingDetailsChildModel> getItems(){
		return items;
	}
}