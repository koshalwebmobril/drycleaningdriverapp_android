package com.wm.muggamudriver.Model.notificationmodel;

import com.google.gson.annotations.SerializedName;


import java.util.List;

public class NotificationResponse {

	@SerializedName("code")
	private int code;

	@SerializedName("notificationList")
	private List<NotificationModel> notificationList;

	@SerializedName("error")
	private boolean error;

	public int getCode(){
		return code;
	}

	public List<NotificationModel> getNotificationList(){
		return notificationList;
	}

	public boolean isError(){
		return error;
	}
}