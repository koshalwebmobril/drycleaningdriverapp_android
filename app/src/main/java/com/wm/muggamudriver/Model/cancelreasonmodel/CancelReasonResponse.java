package com.wm.muggamudriver.Model.cancelreasonmodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.wm.muggamudriver.Model.orderrejectmodel.OrderRejectModel;

public class CancelReasonResponse{

	@SerializedName("code")
	private int code;

	@SerializedName("error")
	private boolean error;
	@SerializedName("message")
	private String message;


	@SerializedName("reasonList")
	private List<OrderRejectModel> reasonList;

	public int getCode(){
		return code;
	}

	public String getMessage(){
		return message;
	}

	public boolean isError(){
		return error;
	}

	public List<OrderRejectModel> getReasonList(){
		return reasonList;
	}
}