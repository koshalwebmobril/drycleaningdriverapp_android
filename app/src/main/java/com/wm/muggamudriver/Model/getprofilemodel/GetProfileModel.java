package com.wm.muggamudriver.Model.getprofilemodel;

import com.google.gson.annotations.SerializedName;

public class GetProfileModel {

	@SerializedName("gender")
	private Object gender;

	@SerializedName("date_of_birth")
	private Object dateOfBirth;

	@SerializedName("latitude")
	private String latitude;

	@SerializedName("open_time")
	private String openTime;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("profile_image_path")
	private String profileImagePath;

	@SerializedName("device_type")
	private String deviceType;

	@SerializedName("type")
	private int type;

	@SerializedName("is_admin")
	private int isAdmin;

	@SerializedName("profile_image_name")
	private Object profileImageName;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("id")
	private int id;

	@SerializedName("email")
	private String email;

	@SerializedName("document_uploaded")
	private int documentUploaded;

	@SerializedName("longitude")
	private String longitude;

	@SerializedName("address")
	private Object address;

	@SerializedName("mobile")
	private String mobile;

	@SerializedName("verified")
	private int verified;

	@SerializedName("email_verified_at")
	private Object emailVerifiedAt;

	@SerializedName("close_time")
	private String closeTime;

	@SerializedName("document_verified")
	private int documentVerified;

	@SerializedName("device_token")
	private String deviceToken;

	@SerializedName("name")
	private String name;

	@SerializedName("is_approved")
	private int isApproved;

	@SerializedName("status")
	private int status;

	public Object getGender(){
		return gender;
	}

	public Object getDateOfBirth(){
		return dateOfBirth;
	}

	public String getLatitude(){
		return latitude;
	}

	public String getOpenTime(){
		return openTime;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public String getProfileImagePath(){
		return profileImagePath;
	}

	public String getDeviceType(){
		return deviceType;
	}

	public int getType(){
		return type;
	}

	public int getIsAdmin(){
		return isAdmin;
	}

	public Object getProfileImageName(){
		return profileImageName;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public int getId(){
		return id;
	}

	public String getEmail(){
		return email;
	}

	public int getDocumentUploaded(){
		return documentUploaded;
	}

	public String getLongitude(){
		return longitude;
	}

	public Object getAddress(){
		return address;
	}

	public String getMobile(){
		return mobile;
	}

	public int getVerified(){
		return verified;
	}

	public Object getEmailVerifiedAt(){
		return emailVerifiedAt;
	}

	public String getCloseTime(){
		return closeTime;
	}

	public int getDocumentVerified(){
		return documentVerified;
	}

	public String getDeviceToken(){
		return deviceToken;
	}

	public String getName(){
		return name;
	}

	public int getIsApproved(){
		return isApproved;
	}

	public int getStatus(){
		return status;
	}
}