package com.wm.muggamudriver.Model.orderdetailsmodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class OrderDetailsResponse {

	@SerializedName("code")
	private int code;

	@SerializedName("itemDetails")
	private List<BookingDetailsParentModel> itemDetails;

	@SerializedName("error")
	private boolean error;

	@SerializedName("bookingDetails")
	private OrderDetailsModel orderDetailsModel;

	public int getCode(){
		return code;
	}

	public List<BookingDetailsParentModel> getItemDetails(){
		return itemDetails;
	}

	public boolean isError(){
		return error;
	}

	public OrderDetailsModel getOrderDetailsModel(){
		return orderDetailsModel;
	}
}