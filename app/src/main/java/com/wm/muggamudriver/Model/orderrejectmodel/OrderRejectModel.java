package com.wm.muggamudriver.Model.orderrejectmodel;

import com.google.gson.annotations.SerializedName;

public class OrderRejectModel {

	@SerializedName("description")
	private String description;

	@SerializedName("id")
	private int id;

	public String getDescription(){
		return description;
	}

	public int getId(){
		return id;
	}
}