package com.wm.muggamudriver;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;

import com.wm.muggamudriver.Activities.LoginActivity;
import com.wm.muggamudriver.Activities.MainActivity;
import com.wm.muggamudriver.Sharedpreference.LoginPreferences;

public class SplashActivity extends AppCompatActivity {

    private final int SPLASH_DISPLAY_LENGTH = 3000;
    Handler handler;
    private Window mWindow;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mWindow = getWindow();
        mWindow.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

        handler = new Handler();
        handler.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                String  value= LoginPreferences.getActiveInstance(SplashActivity.this).getToken();
                Log.d("TAG", value);
                if(value.isEmpty() || value.equals(""))
                {
                    Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                    SplashActivity.this.startActivity(i);
                    SplashActivity.this.finish();
                }
                else
                {
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    SplashActivity.this.startActivity(intent);
                    SplashActivity.this.finish();
                }
            }
        }, SPLASH_DISPLAY_LENGTH);
    }
    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        handler.removeCallbacksAndMessages(null);
        finish();
    }
}