package com.wm.muggamudriver.Utils;

import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class DrawPath
{
    private GoogleMap googleMap;
    int orderStatus=0;
    private double currentLat, currentLong, destinationLat, destinationLong;

    boolean isSecond = false;
    private Polyline lastPolyline;

    public DrawPath(GoogleMap mGoogleMap, double currentLat, double currentLong, double destinationLat, double destinationLong, int orderStatus) {
        this.googleMap = mGoogleMap;
        this.currentLat = currentLat;
        this.currentLong = currentLong;
        this.destinationLat = destinationLat;
        this.destinationLong = destinationLong;
        this.orderStatus = orderStatus;
        Log.e("currentLat---", "-> " + currentLat);
        Log.e("currentLong---", "-> " + currentLong);
        Log.e("destinationLat---", "-> " + destinationLat);
        Log.e("destinationLong---", "-> " + destinationLong);
        Log.e("orderStatus---", "-> " + orderStatus);
        drawPath();
    }

    private void drawPath() {
        /* Curreent Lat-Long */
        LatLng origin = new LatLng(currentLat, currentLong);
        /* Destination Lat-Long */
        LatLng dest = new LatLng(destinationLat, destinationLong);

        // Getting URL to the Google Directions API
        String url = getUrl(/*origin,*/ dest,origin);
        Log.e("drawPath url--", url);
        // Start downloading json data from Google Directions API
        new FetchUrl().execute(url);
        //move map camera
        /*googleMap.addMarker(new MarkerOptions()
                .draggable(false)
                .position(dest)
                .anchor(0.5f, 0.5f)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));*/

    }

    private String getUrl(LatLng origin, LatLng dest) {
        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        // Sensor enabled
        String sensor = "sensor=false";
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;
        // Output format
        String output = "json";
       /* String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters
                + "&key=" + "AIzaSyCnRAGJaYpc4edJi8DcHaimmJ9mW4k4EVM";
*/
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters
                + "&key=" + "AIzaSyBIh2QYMeUhlYaxusM7BVp9w3185A4doJA";

        Log.d("url",url);
        return url;
    }

    private class FetchUrl extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... url) {
            // For storing data from web service
            String data = "";
            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                Log.e("Background Task data", data);
            } catch (Exception e) {
                Log.e("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Invokes the thread for parsing the JSON data
            try {
                new ParserTask().execute(result);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);
            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();
            // Connecting to url
            urlConnection.connect();
            // Reading data from url
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuffer sb = new StringBuffer();
            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
            Log.e("downloadUrl", data);
            br.close();

        } catch (Exception e) {
            Log.e("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {
        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {
            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;
            try {
                jObject = new JSONObject(jsonData[0]);
                Log.e("ParserTask", jsonData[0]);

               DataParser parser = new DataParser();
                Log.e("ParserTask", parser.toString());

                // Starts parsing data
                routes = parser.parse(jObject);
                Log.e("ParserTask", "Executing routes");
                Log.e("ParserTask", routes.toString());

            } catch (Exception e) {
                Log.e("ParserTask", e.toString());
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points=new ArrayList<>();
            PolylineOptions lineOptions =new PolylineOptions() ;
            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
               // points = new ArrayList<>();
               // lineOptions  = new PolylineOptions();
                //Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);
                //Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);
                    double lat = Double.parseDouble(Objects.requireNonNull(point.get("lat")));
                    double lng = Double.parseDouble(Objects.requireNonNull(point.get("lng")));
                    LatLng position = new LatLng(lat, lng);
                    points.add(position);
                }
                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(8);
                //lineOptions.color(Color.parseColor("#F05A24"));
                lineOptions.color(Color.BLACK);
               // googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(destinationLat, destinationLong), 15.0f));
                if (orderStatus==3){
                    //googleMap.animateCamera(CameraUpdateFactory.zoomTo(googleMap.getCameraPosition().zoom - 0.5f));
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(destinationLat, destinationLong), 13.0f));
                }else {
                    LatLng mDestination=new LatLng(destinationLat,destinationLong);
                    LatLng mOrigin=new LatLng(currentLat,currentLong);
                    LatLngBounds.Builder builder = new LatLngBounds.Builder();
                    builder.include(mDestination).include(mOrigin);
                    LatLngBounds latLngBounds = builder.build();
                      /*LatLngBounds latLngBounds = new LatLngBounds(
                            new LatLng(destinationLat, destinationLong), new LatLng(currentLat, currentLong));*/
                    // googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(destinationLat, destinationLong), 15.0f));
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 70));
                }
                //googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(destinationLat, destinationLong), 0));
                Log.e("onPostExecute", "onPostExecute lineoptions decoded");
            }
            // Drawing polyline in the Google Map for the i-th route
            if (lineOptions != null) {
                if(isSecond) {
                    lastPolyline.remove();
                    lastPolyline = googleMap.addPolyline(lineOptions);
                } else {
                    lastPolyline = googleMap.addPolyline(lineOptions);
                    isSecond=true;
                }
               // googleMap.addPolyline(lineOptions);
            } else {
                Log.e("onPostExecute", "without Polylines drawn");
            }
        }
    }

    public static class DataParser
    {
        public List<List<HashMap<String, String>>> parse(JSONObject jObject)
        {
            List<List<HashMap<String, String>>> routes = new ArrayList<>();
            JSONArray jRoutes;
            JSONArray jLegs;
            JSONArray jSteps;
            JSONObject jDistance,jDuration;
            try
            {
                jRoutes = jObject.getJSONArray("routes");
                /** Traversing all routes */
                for (int i = 0; i < jRoutes.length(); i++)
                {
                    jLegs = ((JSONObject) jRoutes.get(i)).getJSONArray("legs");
                    jDistance = ((JSONObject) jLegs.get(i)).getJSONObject("distance");
                    jDuration = ((JSONObject) jLegs.get(i)).getJSONObject("duration");
                   /* distance=jDistance.getString("text");
                    duration=jDuration.getString("text");*/
                    List path = new ArrayList<>();

                    /** Traversing all legs */
                    for (int j = 0; j < jLegs.length(); j++) {
                        jSteps = ((JSONObject) jLegs.get(j)).getJSONArray("steps");


                        /** Traversing all steps */
                        for (int k = 0; k < jSteps.length(); k++) {
                            String polyline = "";
                            polyline = (String) ((JSONObject) ((JSONObject) jSteps.get(k)).get("polyline")).get("points");
                            List<LatLng> list = decodePoly(polyline);

                            /** Traversing all points */
                            for (int l = 0; l < list.size(); l++) {
                                HashMap<String, String> hm = new HashMap<>();
                                hm.put("lat", Double.toString((list.get(l)).latitude));
                                hm.put("lng", Double.toString((list.get(l)).longitude));
                                path.add(hm);
                            }
                        }
                        routes.add(path);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
            }
            return routes;
        }

        private List<LatLng> decodePoly(String encoded) {

            List<LatLng> poly = new ArrayList<>();
            int index = 0, len = encoded.length();
            int lat = 0, lng = 0;

            while (index < len) {
                int b, shift = 0, result = 0;
                do {
                    b = encoded.charAt(index++) - 63;
                    result |= (b & 0x1f) << shift;
                    shift += 5;
                } while (b >= 0x20);
                int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
                lat += dlat;

                shift = 0;
                result = 0;
                do {
                    b = encoded.charAt(index++) - 63;
                    result |= (b & 0x1f) << shift;
                    shift += 5;
                } while (b >= 0x20);
                int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
                lng += dlng;

                LatLng p = new LatLng((((double) lat / 1E5)),
                        (((double) lng / 1E5)));
                poly.add(p);
            }

            return poly;
        }
    }
}
