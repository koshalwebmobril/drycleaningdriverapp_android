package com.wm.muggamudriver.Utils;

public class UrlApi
{
   // public static String BASE_URL = "https://webmobril.org/dev/dryCleaning/api/driver/";   //webmobril server

    public static String BASE_URL = "http://52.54.1.108/drycleaning/api/driver/";       // Aws Server
    public static String BASE_URL1 = "http://52.54.1.108/drycleaning/api/v1/";       // Aws contact us
    public static String BASE_URL2 = "http://52.54.1.108/drycleaning/driver/";       // Aws privacy policy

    public static final String REGISTER="register";
    public static final String LOGIN="login";
    public static final String VERIFYOTP="verify-otp";
    public static final String FORGOTPASSWORD="forgot-password";
    public static final String RESETPASSWORD="reset-password";
    public static final String GETPROFILE="get-profile";
    public static final String UPDATEPROFILE="update-profile";
    public static final String CHANGEPASSWORD=" change-password";
    public static final String CONTACTUS="contact-us";
    public static final String GETNOTIFICATION="notifications";
    public static final String ALLORDER="all-orders";
    public static final String ALLACCEPTEDORDER="accepted-orders";
    public static final String ALLREJECTEDORDER="rejected-orders";
    public static final String POPUPDATA="notification-details";
    public static final String ACCEPTBOOKING="accept-order";
    public static final String REJECTBOOKING="reject-order";
    public static final String ORDERDETAILS="order-details";
    public static final String UPDATEORDERSTATUSCHANGE ="update-order-status";
    public static final String CANCELREASON ="cancel-reasons";
    public static final String PRIVACY ="privacy";
    public static final String TERMS="terms";
    public static final String UPDATETOKEN="update-device-token";
    public static final String RESENDOTP="resend-otp";
}
