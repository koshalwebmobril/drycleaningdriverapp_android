package com.wm.muggamudriver.Sharedpreference;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class LoginPreferences
{
    private static com.wm.muggamudriver.Sharedpreference.LoginPreferences preferences = null;
    private static SharedPreferences mPreferences;
    private SharedPreferences.Editor editor;
    private String stateId;
    private Context context;
    private String userid = "userid";
    private  String company_email="email";
    private  String company_name="name";
    private  String profile_image="profile_image";
    private  String providerid="providerid";
    private  String token="token";
    private  String cancel_id="cancel_id";





    public LoginPreferences(Context context) {
        this.context = context;
        setmPreferences(PreferenceManager.getDefaultSharedPreferences(context));
    }

    public SharedPreferences getmPreferences() {
        return mPreferences;
    }

    private void setmPreferences(SharedPreferences mPreferences) {
        this.mPreferences = mPreferences;
    }

    public static com.wm.muggamudriver.Sharedpreference.LoginPreferences getActiveInstance(Context context)
    {
        if (preferences == null) {
            preferences = new com.wm.muggamudriver.Sharedpreference.LoginPreferences(context);
        }
        return preferences;
    }

    public String getToken()
    {
        return mPreferences.getString(this.token, "");
    }

    public void setToken(String token)
    {
        editor = mPreferences.edit();
        editor.putString(this.token, token);
        editor.apply();
    }

    public void setUserName(String name)
    {
        editor = mPreferences.edit();
        editor.putString(this.company_name, name);
        editor.apply();
    }

    public String getUserName()
    {
        return mPreferences.getString(this.company_name, "");
    }

    public void setUserEmail(String email)
    {
        editor = mPreferences.edit();
        editor.putString(this.company_email, email);
        editor.apply();
    }

    public String getCancelId()
    {
        return mPreferences.getString(this.cancel_id, "");
    }

    public void setCancelId( String cancel_id)
    {
        editor = mPreferences.edit();
        editor.putString(this.cancel_id, cancel_id);
        editor.apply();
    }

    public String getUserEmail()
    {
        return mPreferences.getString(this.company_email, "");
    }

    public static void deleteAllPreference()
    {
        mPreferences.edit().clear().apply();
    }


    public String getUserProfile()
    {
        return mPreferences.getString(this.profile_image, "");
    }
    public void setUserProfile(String profile_image)
    {
        editor = mPreferences.edit();
        editor.putString(this.profile_image, profile_image);
        editor.apply();
    }


    public String getProviderId()
    {
        return mPreferences.getString(this.providerid, "");
    }


    public void setProviderId(String providerid)
    {
        editor = mPreferences.edit();
        editor.putString(this.providerid, providerid);
        editor.apply();
    }


}
