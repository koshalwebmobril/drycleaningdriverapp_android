package com.wm.muggamudriver.network;



import com.wm.muggamudriver.Model.acceptbookingmodel.AcceptBookingResponse;
import com.wm.muggamudriver.Model.allordermodel.AllOrderResponse;
import com.wm.muggamudriver.Model.cancelreasonmodel.CancelReasonResponse;
import com.wm.muggamudriver.Model.orderdetailsmodel.OrderDetailsResponse;
import com.wm.muggamudriver.Model.changepasswordmodel.ChangePasswordResponse;
import com.wm.muggamudriver.Model.contactusmodel.ContactusResponse;
import com.wm.muggamudriver.Model.forgotpasswordmodel.ForgotPasswordResponse;
import com.wm.muggamudriver.Model.getprofilemodel.GetProfileResponse;
import com.wm.muggamudriver.Model.loginmodel.Login1Response;
import com.wm.muggamudriver.Model.notificationmodel.NotificationResponse;
import com.wm.muggamudriver.Model.otpverifiymodel.OtpVerifiyResponse;
import com.wm.muggamudriver.Model.popupdatamodel.PopUpDataResponse;
import com.wm.muggamudriver.Model.registerresponse.RegisterResponse;
import com.wm.muggamudriver.Model.orderrejectmodel.RejectBookingResponse;
import com.wm.muggamudriver.Model.resetpasswordmodel.ResetPasswordResponse;
import com.wm.muggamudriver.Model.updateorderstatusmodel.UpdateOrderStatusResponse;
import com.wm.muggamudriver.Model.updateprofilemodel.UpdateProfileResponse;
import com.wm.muggamudriver.Utils.UrlApi;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiInterface
{
    @FormUrlEncoded
    @POST(UrlApi.LOGIN)
    Call<Login1Response> LoginUser(@Field("email") String email,
                                   @Field("password") String password,
                                   @Field("type") String type,
                                   @Field("device_type") String deviceType,
                                   @Field("device_token") String deviceToken,
                                   @Field("latitude") String latitude,
                                   @Field("longitude") String longitude);

    @FormUrlEncoded
    @POST(UrlApi.REGISTER)
    Call<RegisterResponse> RegisterUser(@Field("name") String username,
                                        @Field("email") String email,
                                        @Field("mobile") String mobile,
                                        @Field("type") String type,
                                        @Field("password") String password,
                                        @Field("device_type") String deviceType,
                                        @Field("device_token") String deviceToken,
                                        @Field("latitude") String latitude,
                                        @Field("longitude") String longitude);

    @FormUrlEncoded
    @POST(UrlApi.VERIFYOTP)
    Call<OtpVerifiyResponse> OtpVerifiey(@Field("email") String email,
                                         @Field("otp") String otp);

    @FormUrlEncoded
    @POST(UrlApi.RESETPASSWORD)
    Call<ResetPasswordResponse> ResetPassword (@Field("email") String email,
                                               @Field("type") String type,
                                               @Field("password") String password);

    @FormUrlEncoded
    @POST(UrlApi.FORGOTPASSWORD)
    Call<ForgotPasswordResponse> ForgotPassword(@Field("email") String email,
                                                @Field("type") String type);


    @FormUrlEncoded
    @POST(UrlApi.RESENDOTP)
    Call<ForgotPasswordResponse> Resendotp(@Field("email") String email,
                                                @Field("type") String type);




    @GET(UrlApi.GETPROFILE)
    Call<GetProfileResponse> GetProfile(@Header("Authorization") String token);


    @GET(UrlApi.CANCELREASON)
    Call<CancelReasonResponse> GetReason(@Header("Authorization") String token);




    @Multipart
    @POST(UrlApi.UPDATEPROFILE)
    Call<UpdateProfileResponse> updateprofileresponse(@Header("Authorization") String token,
                                                      @Part("name") RequestBody name,
                                                      @Part("address") RequestBody emailaddress,
                                                      @Part MultipartBody.Part image,
                                                      @Part("latitude") RequestBody latitude,
                                                      @Part("longitude") RequestBody longitude);


    @FormUrlEncoded
    @POST(UrlApi.CHANGEPASSWORD)
    Call<ChangePasswordResponse> changepassword(@Header("Authorization") String token, @Field("old_password") String old_password, @Field("password") String password);

    @FormUrlEncoded
    @POST(UrlApi.REJECTBOOKING)
    Call<RejectBookingResponse> rejectBooking(@Header("Authorization") String token, @Field("booking_id") String booking_id, @Field("cancel_reason_id") String cancel_reason_id);



    @FormUrlEncoded
    @POST(UrlApi.CONTACTUS)
    Call<ContactusResponse> contactus(@Header("Authorization") String token, @Field("subject") String subject, @Field("comments") String comments);

    @FormUrlEncoded
    @POST(UrlApi.ORDERDETAILS)
    Call<OrderDetailsResponse> orderDetails(@Header("Authorization") String token, @Field("booking_id") String booking_id);


    @FormUrlEncoded
    @POST(UrlApi.UPDATEORDERSTATUSCHANGE)
    Call<UpdateOrderStatusResponse>UpdateOrderstatus(@Header("Authorization") String token, @Field("booking_id") String booking_id,@Field("booking_status") String booking_status);




    @FormUrlEncoded
    @POST(UrlApi.ACCEPTBOOKING)
    Call<AcceptBookingResponse> acceptBooking(@Header("Authorization") String token, @Field("booking_id") String booking_id);


    @FormUrlEncoded
    @POST(UrlApi.POPUPDATA)
    Call<PopUpDataResponse> popupData(@Header("Authorization") String token, @Field("booking_id") String booking_id);

    @GET(UrlApi.GETNOTIFICATION)
    Call<NotificationResponse> getNotification(@Header("Authorization") String token);

    @GET(UrlApi.ALLORDER)
    Call<AllOrderResponse> getAllOrder(@Header("Authorization") String token);

    @GET(UrlApi.ALLACCEPTEDORDER)
    Call<AllOrderResponse> getAllAcceptedOrder(@Header("Authorization") String token);

    @GET(UrlApi.ALLREJECTEDORDER)
    Call<AllOrderResponse> getAllRejectedOrder(@Header("Authorization") String token);


    @FormUrlEncoded
    @POST(UrlApi.UPDATETOKEN)
    Call<ResetPasswordResponse> updatetoken(@Header("Authorization") String token ,
                                            @Field("device_type") String deviceType,
                                            @Field("device_token") String deviceToken);
}
