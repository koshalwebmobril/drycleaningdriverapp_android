package com.wm.muggamudriver.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.wm.muggamudriver.Interface.NotificationClick;
import com.wm.muggamudriver.Model.notificationmodel.NotificationModel;
import com.wm.muggamudriver.R;

import java.util.List;
public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder>
{
    Context context;
    List<NotificationModel> notificationModelList;
    NotificationClick notificationclick;

    public NotificationAdapter(Context context, List notificationModelList,NotificationClick notificationClick)
    {
        this.context = context;
        this.notificationModelList=notificationModelList;
        this.notificationclick=notificationClick;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
         context = holder.itemView.getContext();
         NotificationModel notificationmodel = notificationModelList.get(position);
         holder.title.setText(notificationmodel.getTitle());
         holder.title_text.setText(notificationmodel.getDescription());
         holder.timing.setText(notificationmodel.getBookedAt());

        holder.itemView.setOnClickListener(view ->
        {
            notificationclick.NotificationPosition(position,notificationmodel.getBookingId());
        });
    }
    @Override
    public int getItemCount()
    {
        return notificationModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView title,title_text,timing;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            title=itemView.findViewById(R.id.title);
            title_text=itemView.findViewById(R.id.title_text);
            timing=itemView.findViewById(R.id.timing);
        }
    }
}