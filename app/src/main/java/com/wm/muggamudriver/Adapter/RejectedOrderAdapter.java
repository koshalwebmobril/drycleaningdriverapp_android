package com.wm.muggamudriver.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.wm.muggamudriver.Activities.BookingDetailsActivity;
import com.wm.muggamudriver.Activities.GetDirectionActivity;
import com.wm.muggamudriver.Model.allordermodel.AllOrderModel;
import com.wm.muggamudriver.R;

import java.util.List;

public class RejectedOrderAdapter extends RecyclerView.Adapter<RejectedOrderAdapter.MyViewHolder>
{
    List<AllOrderModel> allservicemodellist;
    Context context;
    public RejectedOrderAdapter(Context context, List allservicemodellist)
    {
        this.context = context;
        this.allservicemodellist = allservicemodellist;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rejected, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        final AllOrderModel getallbookingmodel = allservicemodellist.get(position);

        Glide.with(context)
                .load(getallbookingmodel.getUserProfileImage()).apply(new RequestOptions().placeholder(R.drawable.profile).error(R.drawable.profile))
                .into(holder.user_image);
        holder.user_name.setText(getallbookingmodel.getUserName());
        holder.time.setText(getallbookingmodel.getBookedAt());
        holder.user_mobile_number.setText(getallbookingmodel.getUserMobile());
        holder.register_no.setText(getallbookingmodel.getOrderId());
        holder.address.setText(getallbookingmodel.getPickupLocation());
        holder.date_and_time.setText(getallbookingmodel.getPickupDate());
        holder.service_time.setText(getallbookingmodel.getPickupTime());


        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i=new Intent(context, BookingDetailsActivity.class);
                i.putExtra("booking_id",String.valueOf(getallbookingmodel.getId()));
                i.putExtra("user_name",holder.user_name.getText().toString().trim());
                i.putExtra("pickup_time",getallbookingmodel.getPickupTime());
                i.putExtra("pickupdate",getallbookingmodel.getPickupDate());
                i.putExtra("pickuplocation",getallbookingmodel.getPickupLocation());

                i.putExtra("delivery_date",getallbookingmodel.getDeliveryDate());
                i.putExtra("delivery_time",getallbookingmodel.getDeliveryTime());
                i.putExtra("delivery_location",getallbookingmodel.getDropLocation());
                i.putExtra("rejected_status","1");

                i.putExtra("profile_image",String.valueOf(getallbookingmodel.getUserProfileImage()));
                i.putExtra("phone_no",holder.user_mobile_number.getText().toString().trim());
                context.startActivity(i);
            }
        });

        holder.relative_getdirection.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                if(getallbookingmodel.getBookingStatus()==7)
                {
                    Intent i=new Intent(context, GetDirectionActivity.class);
                    i.putExtra("latitude",getallbookingmodel.getProviderLatitude());
                    i.putExtra("longitude",getallbookingmodel.getProviderLongitude());
                    context.startActivity(i);
                }
                else
                {
                    Intent i=new Intent(context, GetDirectionActivity.class);
                    i.putExtra("latitude",getallbookingmodel.getUserLatitude());
                    i.putExtra("longitude",getallbookingmodel.getUserLongitude());
                    context.startActivity(i);
                }

            }
        });


    }
    @Override
    public int getItemCount() {
        return allservicemodellist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView status,txtstatus,user_name,time,user_mobile_number,register_no,service_name,Price,date_and_time,service_time,address;
        ImageView user_image;
        RelativeLayout relative_getdirection;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            user_image=itemView.findViewById(R.id.user_image);
            user_name=itemView.findViewById(R.id.user_name);
            time=itemView.findViewById(R.id.time);
            user_mobile_number=itemView.findViewById(R.id.user_mobile_number);
            register_no=itemView.findViewById(R.id.register_no);
            service_name=itemView.findViewById(R.id.service_name);
            Price=itemView.findViewById(R.id.Price);
            date_and_time=itemView.findViewById(R.id.date_and_time);
            service_time=itemView.findViewById(R.id.service_time);
            address=itemView.findViewById(R.id.address);
            txtstatus=itemView.findViewById(R.id.txtstatus);
            relative_getdirection=itemView.findViewById(R.id.Relative_getdirection);
        }
    }
}