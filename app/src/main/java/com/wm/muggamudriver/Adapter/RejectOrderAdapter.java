package com.wm.muggamudriver.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.wm.muggamudriver.Model.orderrejectmodel.OrderRejectModel;
import com.wm.muggamudriver.R;
import com.wm.muggamudriver.Sharedpreference.LoginPreferences;

import java.util.List;

public class RejectOrderAdapter extends RecyclerView.Adapter<RejectOrderAdapter.MyViewHolder>
{
    List<OrderRejectModel> allservicemodellist;
    Context context;
    private RadioGroup lastCheckedRadioGroup = null;
    public RejectOrderAdapter(Context context, List allservicemodellist)
    {
        this.context = context;
        this.allservicemodellist = allservicemodellist;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rejectorder, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
            OrderRejectModel getallbookingmodel = allservicemodellist.get(position);
            int selectedid = (position+1);
            RadioButton rb = new RadioButton(RejectOrderAdapter.this.context);
            String description=getallbookingmodel.getDescription();
            rb.setId(selectedid++);
            rb.setText(description);
            holder.radioGroup.addView(rb);
        if(position==0)
        {
            rb.setChecked(true);
            selectedid=allservicemodellist.get(0).getId();
            LoginPreferences.getActiveInstance(context).setCancelId(String.valueOf(selectedid));
           /* Toast.makeText(context,
                    "Radio button clicked " + selectedid,
                    Toast.LENGTH_SHORT).show();*/
        }
    }
    @Override
    public int getItemCount() {
        return allservicemodellist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        RadioGroup radioGroup;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            radioGroup=itemView.findViewById(R.id.radiogroup);
            radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i)
                {
                    if (lastCheckedRadioGroup != null
                            && lastCheckedRadioGroup.getCheckedRadioButtonId()
                            != radioGroup.getCheckedRadioButtonId()
                            && lastCheckedRadioGroup.getCheckedRadioButtonId() != -1)
                    {
                        lastCheckedRadioGroup.clearCheck();
                        LoginPreferences.getActiveInstance(context).setCancelId(String.valueOf(radioGroup.getCheckedRadioButtonId()));
                        /* Toast.makeText(context,
                                "Radio button clicked " + radioGroup.getCheckedRadioButtonId(),
                                Toast.LENGTH_SHORT).show();*/

                    }
                    lastCheckedRadioGroup = radioGroup;
                }
            });
        }
    }
}