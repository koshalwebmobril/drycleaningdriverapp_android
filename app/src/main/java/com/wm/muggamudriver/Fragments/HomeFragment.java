package com.wm.muggamudriver.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.wm.muggamudriver.Activities.MainActivity;
import com.wm.muggamudriver.Adapter.ServicesAdapterViewPager;
import com.wm.muggamudriver.R;
import com.wm.muggamudriver.databinding.FragmentHomeBinding;


import java.util.ArrayList;

public class HomeFragment extends Fragment implements View.OnClickListener
{
    private FragmentHomeBinding binding;
    private FragmentTransaction ft;
    private Fragment currentFragment;

    ViewPager simpleViewPager;
    TabLayout tabLayout;
    private ArrayList<Fragment> fragmentArrayList;
    private ArrayList<String> titleList;
    View view;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        view = binding.getRoot();
        ((MainActivity)requireActivity()).toolbarHome();
        init();

        fragmentArrayList.add(new AllOrderFragment());
        titleList.add(getString(R.string.allorder));

        fragmentArrayList.add(new OrderAcceptedFragment());
        titleList.add(getString(R.string.accepted));
        tabLayout.setupWithViewPager(simpleViewPager);
        simpleViewPager.setOffscreenPageLimit(1);

        fragmentArrayList.add(new OrderRejectedFragment());
        titleList.add(getString(R.string.rejected));

       tabLayout.setupWithViewPager(simpleViewPager);
        simpleViewPager.setOffscreenPageLimit(2);

        ServicesAdapterViewPager adapter = new ServicesAdapterViewPager(getActivity().getSupportFragmentManager(), fragmentArrayList,titleList);
        simpleViewPager.setAdapter(adapter);
        simpleViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        simpleViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels
            ) {
                try
                { }
                catch (Exception e){e.printStackTrace();}
            }

            @Override
            public void onPageSelected(int position) {
                try{
                    final InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(simpleViewPager.getWindowToken(), 0);
                }
                catch (Exception e){e.printStackTrace();}
            }


            @Override
            public void onPageScrollStateChanged(int state)
            {
                try
                {

                }
                catch (Exception e){e.printStackTrace();}
            }
        });

       /* simpleViewPager.setOnTouchListener(new View.OnTouchListener()
        {
            public boolean onTouch(View arg0, MotionEvent arg1)
            {
                return true;
            }
        });
*/
        return view;
    }

    private void init()
    {
        simpleViewPager = (ViewPager) view.findViewById(R.id.simpleViewPager);
        tabLayout = (TabLayout) view.findViewById(R.id.simpleTabLayout);
        fragmentArrayList=new ArrayList<>();
        titleList=new ArrayList<>();
    }

    @Override
    public void onClick(View v)
    { }

}