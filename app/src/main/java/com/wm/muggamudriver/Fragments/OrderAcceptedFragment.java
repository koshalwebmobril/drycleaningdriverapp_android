package com.wm.muggamudriver.Fragments;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.wm.muggamudriver.Adapter.AcceptedOrderAdapter;
import com.wm.muggamudriver.ApiClient.RetrofitConnection;
import com.wm.muggamudriver.Model.allordermodel.AllOrderModel;
import com.wm.muggamudriver.R;
import com.wm.muggamudriver.Model.allordermodel.AllOrderResponse;
import com.wm.muggamudriver.Sharedpreference.LoginPreferences;
import com.wm.muggamudriver.Utils.ProgressD;
import com.wm.muggamudriver.network.ApiInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class OrderAcceptedFragment extends Fragment implements View.OnClickListener
{
    View view;
    RecyclerView recyclerview;
    AcceptedOrderAdapter acceptedOrderAdapter;
    List<AllOrderModel> allorderlist;
    TextView no_item_message;
    SwipeRefreshLayout mSwipeRefreshLayout;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view= inflater.inflate(R.layout.fragment_all_service, container, false);
        init();
        hitAllAcceptedOrderApi();
      /*  mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh()
            {
                allorderlist.clear();
                mSwipeRefreshLayout.setRefreshing(false);
                hitAllAcceptedOrderApi();
            }
        });*/
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }
    public void init()
    {
        recyclerview=view.findViewById(R.id.recycler);
        no_item_message=view.findViewById(R.id.no_item_message);
      //  mSwipeRefreshLayout=view.findViewById(R.id.swipeToRefresh);

    }
    @Override
    public void onClick(View v)
    { }


    private void hitAllAcceptedOrderApi()
    {
        ProgressD progressDialog = ProgressD.show(getActivity(),getResources().getString(R.string.logging_in), true, false, null);
        ApiInterface service = RetrofitConnection.getInstance().createService();
        Call<AllOrderResponse> call = service.getAllAcceptedOrder(LoginPreferences.getActiveInstance(getActivity()).getToken());
        call.enqueue(new Callback<AllOrderResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<AllOrderResponse> call, retrofit2.Response<AllOrderResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    AllOrderResponse resultFile = response.body();
                    if(resultFile.getCode() == 200)
                    {
                        no_item_message.setVisibility(View.GONE);
                        recyclerview.setVisibility(View.VISIBLE);
                        allorderlist=resultFile.getBookings();

                        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity(),RecyclerView.VERTICAL,false);
                        recyclerview.setLayoutManager(mLayoutManager);
                        acceptedOrderAdapter = new AcceptedOrderAdapter(getActivity(),allorderlist);
                        recyclerview.setAdapter(acceptedOrderAdapter);
                    }

                    else if(resultFile.getCode() == 404)
                    {
                        no_item_message.setVisibility(View.VISIBLE);
                        no_item_message.setText(resultFile.getMessage());
                        recyclerview.setVisibility(View.GONE);
                    }
                    else
                    {
                        // Toast.makeText(LoginActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<AllOrderResponse> call, Throwable t)
            {
                Toast.makeText(getActivity(), "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }
}