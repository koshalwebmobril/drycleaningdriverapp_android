package com.wm.muggamudriver.Activities;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.wm.muggamudriver.R;
import com.wm.muggamudriver.Utils.DrawPath;
import com.wm.muggamudriver.Utils.GPSTracker;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.ContentValues.TAG;

public class GetDirectionActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.relative_toolbar)
    RelativeLayout relativeToolbar;
    @BindView(R.id.relative_details)
    RelativeLayout relativeDetails;



    @BindView(R.id.txtdroplocation)
    TextView txtdroplocation;

    SupportMapFragment mapFragment;
    ArrayList<LatLng> mMarkerPoints;
    GPSTracker gpsTracker;
    private static final int PERMISSION_REQ_CODE = 1<<2;
    private String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION,};
    private GoogleMap mMap;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    private double current_location_longitude;
    private double current_location_latitude;
    LocationRequest mLocationRequest;
    private LatLng latLng_origin;
    Marker mCurrLocationMarker;
        Marker mDestinationLocationMarker;
    private LatLng mDestination;
    int orderStatus=0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_direction);
        ButterKnife.bind(this);
        checkPermission();
        mapFragment = (SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mMarkerPoints = new ArrayList<>();
    }

    @OnClick(R.id.back)
    public void onClick()
    {
        finish();
    }
    @Override
    public void onConnected(@Nullable Bundle bundle)
    {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000000);
        mLocationRequest.setFastestInterval(1000000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(GetDirectionActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location)
    {
        try
        {
            mLastLocation = location;
            current_location_latitude = location.getLatitude();
            current_location_longitude = location.getLongitude();
            latLng_origin = new LatLng(location.getLatitude(), location.getLongitude());
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng_origin, 11));

            double destination_lat = Double.parseDouble(getIntent().getStringExtra("latitude"));
            double destination_lng = Double.parseDouble(getIntent().getStringExtra("longitude"));
            /*double destination_lat = Double.parseDouble("25.2138");
            double destination_lng = Double.parseDouble("75.8648");*/

            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());
            addresses = geocoder.getFromLocation(destination_lat, destination_lng, 1);
            String address = addresses.get(0).getAddressLine(0);
            txtdroplocation.setText(address);

            DrawPath drawpath = new DrawPath(mMap,current_location_latitude, current_location_longitude,destination_lat,destination_lng,orderStatus);
            Log.d(TAG, "latlng" +latLng_origin);
            if(mCurrLocationMarker != null)
            {
                mCurrLocationMarker.remove();
            }
            else
            {
                MarkerOptions markerOption1 = new MarkerOptions();
                markerOption1.position(latLng_origin);
                markerOption1.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                mCurrLocationMarker = mMap.addMarker(markerOption1);
            }
            if(mDestinationLocationMarker !=null)
            {
                mDestinationLocationMarker.remove();
            }
            else
            {
                mDestination = new LatLng(destination_lat, destination_lng);
                MarkerOptions markerOption2 = new MarkerOptions();
                markerOption2.position(mDestination);
                markerOption2.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                mCurrLocationMarker = mMap.addMarker(markerOption2);
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mMap = googleMap;
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(GetDirectionActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                //Location Permission already granted
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
            else
            {
                checkPermission();
            }
        }
        else
        {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }
    }

    protected synchronized void buildGoogleApiClient()
    {
        mGoogleApiClient = new GoogleApiClient.Builder(GetDirectionActivity.this)
                .addConnectionCallbacks(GetDirectionActivity.this)
                .addOnConnectionFailedListener(GetDirectionActivity.this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    private void checkPermission()
    {
        boolean granted = true;
        for (String per : PERMISSIONS) {
            if (!permissionGranted(per))
            {
                granted = false;
                break;
            }
        }
        if (granted)
        {
            checkGPS();
        }
        else
        {
            requestPermissions();
        }
    }
    private boolean permissionGranted(String permission)
    {
        return ContextCompat.checkSelfPermission(
                this, permission) == PackageManager.PERMISSION_GRANTED;
    }
    private void requestPermissions() {
        ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_REQ_CODE);
    }
    private void toastNeedPermissions() {
        Toast.makeText(this, "You Need Accept This Location Permission", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQ_CODE) {
            boolean granted = true;
            for (int result : grantResults)
            {
                granted = (result == PackageManager.PERMISSION_GRANTED);
                if (!granted) break;
            }
            if (granted)
            { }
            else
            {
                toastNeedPermissions();
            }
        }
    }

    private void checkGPS()
    {
        final LocationManager manager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER))
        {
            Toast.makeText(GetDirectionActivity.this, "Please Turn GPS on to continue..", Toast.LENGTH_LONG).show();
        }
        else
        {
            /*gpsTracker=new GPSTracker(this);
            user_lat=String.valueOf(gpsTracker.getLatitude());
            user_lng=String.valueOf(gpsTracker.getLongitude());
            txtaddress.setText(user_lat);*/
        }
    }

    @Override
    public void onPause()
    {
        super.onPause();
        if (mGoogleApiClient != null) {
            try {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, (LocationListener) this);
            } catch (Exception e) {
                // Toast.makeText(getActivity(), "Try Again", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
        mapFragment.onResume();
    }


}