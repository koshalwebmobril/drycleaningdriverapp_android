package com.wm.muggamudriver.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.wm.muggamudriver.ApiClient.RetrofitConnection;
import com.wm.muggamudriver.R;
import com.wm.muggamudriver.Model.forgotpasswordmodel.ForgotPasswordResponse;
import com.wm.muggamudriver.Utils.CommonMethod;
import com.wm.muggamudriver.Utils.ProgressD;
import com.wm.muggamudriver.network.ApiInterface;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;

import static com.wm.muggamudriver.Utils.CommonMethod.isValidEmaillId;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.back_arrow)
    ImageView backArrow;
    @BindView(R.id.imageview)
    ImageView imageview;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.btn_send)
    Button btnSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);
        btnSend.setOnClickListener(this);
        backArrow.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.btn_send:
                if (CommonMethod.isOnline(this))
                {
                    hideKeyboard((Button) v);
                    if (validation())
                    {
                        ForgotPasswordApi();
                    }
                }
                else
                    {
                    CommonMethod.showAlert(getString(R.string.check_internet), this);
                }
                break;

            case R.id.back_arrow:
                finish();
                break;
        }
    }

    public void hideKeyboard(View view) {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception ignored) {
        }
    }

    private boolean validation() {
        if (TextUtils.isEmpty(email.getText().toString().trim()))
        {
            Toast.makeText(this, "Please enter Email id", Toast.LENGTH_SHORT).show();
            return false;
        }

        else if (!isValidEmaillId(email.getText().toString().trim())) {
            Toast.makeText(this, "Please enter valid Email id", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void ForgotPasswordApi()
    {
        final ProgressD progressDialog = ProgressD.show(ForgotPasswordActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        ApiInterface service = RetrofitConnection.getInstance().createService();
        Call<ForgotPasswordResponse> call = service.ForgotPassword(email.getText().toString().trim(), "4");
        call.enqueue(new Callback<ForgotPasswordResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<ForgotPasswordResponse> call, retrofit2.Response<ForgotPasswordResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    ForgotPasswordResponse resultFile = response.body();
                    Toast.makeText(ForgotPasswordActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    if (resultFile.getCode() == 200)
                    {
                        Intent i=new Intent(ForgotPasswordActivity.this,OtpActivity.class);
                        i.putExtra("email",email.getText().toString().trim());
                        i.putExtra("page_status","2");
                        startActivity(i);
                    }
                    else
                    { }
                }
                catch (Exception e)
                {
                    Log.e("Forgot Password  Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<ForgotPasswordResponse> call, Throwable t)
            {
                Toast.makeText(ForgotPasswordActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }
}