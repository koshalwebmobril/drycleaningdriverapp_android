package com.wm.muggamudriver.Activities;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.wm.muggamudriver.ApiClient.RetrofitConnection;
import com.wm.muggamudriver.R;
import com.wm.muggamudriver.Model.changepasswordmodel.ChangePasswordResponse;
import com.wm.muggamudriver.Sharedpreference.LoginPreferences;
import com.wm.muggamudriver.Utils.CommonMethod;
import com.wm.muggamudriver.Utils.ProgressD;
import com.wm.muggamudriver.network.ApiInterface;
import com.xw.repo.XEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;

public class ChangePasswordActivity extends AppCompatActivity {
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.Change_Password)
    TextView ChangePassword;
    @BindView(R.id.relative_toolbar)
    RelativeLayout relativeToolbar;
    @BindView(R.id.profile_image)
    ImageView profileImage;

    @BindView(R.id.btn_changes)
    Button btnChanges;
    @BindView(R.id.old_password)
    XEditText oldPassword;
    @BindView(R.id.new_password)
    XEditText newPassword;
    @BindView(R.id.reset_password)
    XEditText resetPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.back, R.id.btn_changes})
    public void onClick(View view)
    {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.btn_changes:
                if (CommonMethod.isOnline(ChangePasswordActivity.this))
                {
                    hideKeyboard((Button) view);
                    if (validation())
                    {
                        hitchangepasswordApi();
                    }
                } else
                    {
                    CommonMethod.showAlert(getString(R.string.check_internet), ChangePasswordActivity.this);
                }
                break;
        }
    }

    public void hideKeyboard(View view) {
        try {
            InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception ignored) {
        }
    }

    private boolean validation() {
        if (TextUtils.isEmpty(oldPassword.getText().toString().trim())) {
            Toast.makeText(ChangePasswordActivity.this, "Please enter Old Password", Toast.LENGTH_SHORT).show();
            return false;
        } else if (TextUtils.isEmpty(newPassword.getText().toString().trim())) {
            Toast.makeText(ChangePasswordActivity.this, "Please enter New password", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (oldPassword.getText().toString().trim().length() < 6) {
            Toast.makeText(this, "Old Password should be at least 6 digit/characters long", Toast.LENGTH_LONG).show();
            return false;
        } else if (oldPassword.getText().toString().trim().length() > 16) {
            Toast.makeText(this, "Old Password Maximum. 16 characters or digits/characters would be allowed", Toast.LENGTH_LONG).show();
            return false;
        }
        else if (newPassword.getText().toString().trim().length() < 6) {
            Toast.makeText(this, "New Password should be at least 6 digit/characters long", Toast.LENGTH_LONG).show();
            return false;
        } else if (newPassword.getText().toString().trim().length() > 16) {
            Toast.makeText(this, "New Password Maximum. 16 characters or digits/characters would be allowed", Toast.LENGTH_LONG).show();
            return false;
        } else if (TextUtils.isEmpty(resetPassword.getText().toString().trim())) {
            Toast.makeText(ChangePasswordActivity.this, "Please enter Re-Set Password", Toast.LENGTH_SHORT).show();
            return false;
        } else if (resetPassword.getText().toString().trim().length() < 6) {
            Toast.makeText(this, "Reset Password should be at least 6 digit/characters long", Toast.LENGTH_LONG).show();
            return false;
        } else if (resetPassword.getText().toString().trim().length() > 16) {
            Toast.makeText(this, "Reset Password Maximum. 16 characters or digits/characters would be allowed", Toast.LENGTH_LONG).show();
            return false;
        } else if (!resetPassword.getText().toString().trim().equals(newPassword.getText().toString().trim())) {
            Toast.makeText(ChangePasswordActivity.this, "Password and Confirm Password Not Match!", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public void hitchangepasswordApi()
    {
        final ProgressD progressDialog = ProgressD.show(ChangePasswordActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        ApiInterface service = RetrofitConnection.getInstance().createService();
        Call<ChangePasswordResponse> call = service.changepassword(LoginPreferences.getActiveInstance(ChangePasswordActivity.this).getToken(),oldPassword.getText().toString().trim(),newPassword.getText().toString().trim());
        call.enqueue(new Callback<ChangePasswordResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<ChangePasswordResponse> call, retrofit2.Response<ChangePasswordResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    ChangePasswordResponse resultFile = response.body();
                    Toast.makeText(ChangePasswordActivity.this,resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    if(resultFile.getCode() == 200)
                    {
                       /* Intent i=new Intent(ChangePasswordActivity.this,LoginActivity.class);
                        startActivity(i);
                        LoginPreferences.deleteAllPreference();*/
                        finish();
                    }
                    else if(resultFile.getCode()== 401)
                    {
                        Toast.makeText(ChangePasswordActivity.this, "Unauthorized", Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<ChangePasswordResponse> call, Throwable t)
            {
                Toast.makeText(ChangePasswordActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }
}