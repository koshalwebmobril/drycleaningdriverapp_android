package com.wm.muggamudriver.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.wm.muggamudriver.R;
public class AddLocation extends AppCompatActivity
{
    ImageView back;
    Button btn_changes;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_location);
        back=findViewById(R.id.back);
        btn_changes=findViewById(R.id.btn_changes);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btn_changes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Toast.makeText(AddLocation.this, "Location Add Successfully", Toast.LENGTH_SHORT).show();
                Intent i=new Intent(AddLocation.this,MainActivity.class);
                startActivity(i);
            }
        });
    }
}