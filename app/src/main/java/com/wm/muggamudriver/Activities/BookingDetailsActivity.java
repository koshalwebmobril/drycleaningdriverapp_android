package com.wm.muggamudriver.Activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.wm.muggamudriver.Adapter.BookingDetailsParentAdapter;
import com.wm.muggamudriver.ApiClient.RetrofitConnection;
import com.wm.muggamudriver.Model.orderdetailsmodel.BookingDetailsParentModel;
import com.wm.muggamudriver.Model.orderdetailsmodel.OrderDetailsModel;
import com.wm.muggamudriver.R;
import com.wm.muggamudriver.Model.orderdetailsmodel.OrderDetailsResponse;
import com.wm.muggamudriver.Model.updateorderstatusmodel.UpdateOrderStatusResponse;
import com.wm.muggamudriver.Sharedpreference.LoginPreferences;
import com.wm.muggamudriver.Utils.ProgressD;
import com.wm.muggamudriver.network.ApiInterface;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

public class BookingDetailsActivity extends AppCompatActivity
{
    @BindView(R.id.imgback)
    ImageView imgback;
    @BindView(R.id.relative_toolbar)
    RelativeLayout relativeToolbar;
    @BindView(R.id.user_image)
    CircleImageView userImage;
    @BindView(R.id.provider_name)
    TextView providerName;
    @BindView(R.id.contact_no)
    TextView contactNo;
    @BindView(R.id.created_date)
    TextView createdDate;
    @BindView(R.id.order_no)
    TextView orderNo;
    @BindView(R.id.recyclerview_yourbooking)
    RecyclerView recyclerviewYourbooking;
    @BindView(R.id.txt_delivery_amount)
    TextView txtDeliveryAmount;
    @BindView(R.id.total_amount)
    TextView totalAmount;
    @BindView(R.id.relative_total_amount)
    RelativeLayout relativeTotalAmount;
    @BindView(R.id.linear_service_details)
    LinearLayout linearServiceDetails;
    @BindView(R.id.txt_pickup_date)
    TextView txt_pickup_date;
    @BindView(R.id.txt_pickuptime)
    TextView txtPickuptime;
    @BindView(R.id.txt_pickupaddress)
    TextView txtPickupaddress;
    @BindView(R.id.txtdelivery_date)
    TextView txtdeliveryDate;
    @BindView(R.id.txt_delivery_time)
    TextView txtDeliveryTime;
    @BindView(R.id.status)
    TextView status;
    BookingDetailsParentAdapter bookingDetailsParentAdapter;
    @BindView(R.id.driver_status)
    TextView driverStatus;

    @BindView(R.id.linear_mobileno)
    LinearLayout linear_mobileno;
    private int booking_status;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_details1);
        ButterKnife.bind(this);
        getBookingDetailsApi();

        driverStatus.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                PopupMenu popup = new PopupMenu(BookingDetailsActivity.this, v, Gravity.END);
               // popup.inflate(R.menu.poupup_menu);
                if(booking_status==7)
                {
                    popup.getMenu().add(1, R.id.Pickup, 1, "Pickup");
                }
                else if(booking_status==10)
                {
                    popup.getMenu().add(1, R.id.Pickup, 1, "Pickup");
                }
                else if(booking_status==8)
                {
                    popup.getMenu().add(1, R.id.Deliver, 1, "Deliver");
                }
                else if(booking_status==11)
                {
                    popup.getMenu().add(1, R.id.Deliver, 1, "Deliver");
                }
                else
                {
                    driverStatus.setVisibility(GONE);
                }

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener()
                {
                    @Override
                    public boolean onMenuItemClick(MenuItem item)
                    {
                        switch (item.getItemId())
                        {
                            case R.id.Pickup:
                                hitupdateorderstatusapi("1");
                                break;

                            case R.id.Deliver:
                                hitupdateorderstatusapi("2");
                                break;
                        }
                        return false;
                    }
                });
                popup.show();
            }
        });

        linear_mobileno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", contactNo.getText().toString().trim(), null));
                startActivity(intent);
            }
        });

    }

    @OnClick(R.id.imgback)
    public void onClick()
    {
        finish();
    }

    private void getBookingDetailsApi()
    {
        final ProgressD progressDialog = ProgressD.show(BookingDetailsActivity.this, getResources().getString(R.string.logging_in), true, false, null);
        ApiInterface service = RetrofitConnection.getInstance().createService();
        Call<OrderDetailsResponse> call = service.orderDetails(LoginPreferences.getActiveInstance(this).getToken(), getIntent().getStringExtra("booking_id"));
        call.enqueue(new Callback<OrderDetailsResponse>() {


            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<OrderDetailsResponse> call, Response<OrderDetailsResponse> response) {
                progressDialog.dismiss();
                try {
                    OrderDetailsResponse resultFile = response.body();
                    // Toast.makeText(BookingDetails.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    if(resultFile.getCode() == 200)
                    {

                        linearServiceDetails.setVisibility(View.VISIBLE);
                        OrderDetailsModel bookingdetails = resultFile.getOrderDetailsModel();

                        if(getIntent().getExtras()!=null)
                        {
                            Glide.with(BookingDetailsActivity.this)
                                    .load(getIntent().getStringExtra("profile_image")).apply(new RequestOptions().placeholder(R.drawable.profile).error(R.drawable.profile))
                                    .into(userImage);
                            providerName.setText(getIntent().getStringExtra("user_name"));
                            txtPickuptime.setText(getIntent().getStringExtra("pickup_time"));
                            txt_pickup_date.setText(getIntent().getStringExtra("pickupdate"));
                            txtPickupaddress.setText(getIntent().getStringExtra("pickuplocation"));

                            txtdeliveryDate.setText(getIntent().getStringExtra("delivery_date"));
                            txtDeliveryTime.setText(getIntent().getStringExtra("delivery_time"));
                            contactNo.setText(getIntent().getStringExtra("phone_no"));
                        }
                        orderNo.setText("Order No -: " + bookingdetails.getOrderId());
                        String total_amount_string = String.format("%.2f", bookingdetails.getTotalAmount());
                        totalAmount.setText(total_amount_string);
                        String deliver_charge = String.format("%.2f", bookingdetails.getDeliveryFee());
                        txtDeliveryAmount.setText(deliver_charge);

                        List<BookingDetailsParentModel> parentlist = resultFile.getItemDetails();
                        LinearLayoutManager mLayoutManager = new LinearLayoutManager(BookingDetailsActivity.this, RecyclerView.VERTICAL, false);
                        recyclerviewYourbooking.setLayoutManager(mLayoutManager);
                        bookingDetailsParentAdapter = new BookingDetailsParentAdapter(BookingDetailsActivity.this, parentlist);
                        recyclerviewYourbooking.setAdapter(bookingDetailsParentAdapter);
                         booking_status=bookingdetails.getBookingStatus();

                         if(getIntent().getStringExtra("rejected_status").equals("2"))  //all order and accepted condition
                         {
                             if(getIntent().getStringExtra("driver_pickup").equals("1") || getIntent().getStringExtra("driver_deliver").equals("1"))
                             {
                                 driverStatus.setVisibility(GONE);
                             }
                             else
                             {
                                 driverStatus.setVisibility(View.VISIBLE);
                             }
                         }
                         else
                         {
                             driverStatus.setVisibility(GONE);
                         }


                    }
                    else
                        {
                        // Toast.makeText(LoginActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                       }
                } catch (Exception e) {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<OrderDetailsResponse> call, Throwable t) {
                Toast.makeText(BookingDetailsActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    private void hitupdateorderstatusapi(String orderstatus)
    {
        final ProgressD progressDialog = ProgressD.show(BookingDetailsActivity.this, getResources().getString(R.string.logging_in), true, false, null);
        ApiInterface service = RetrofitConnection.getInstance().createService();
        Call<UpdateOrderStatusResponse> call = service.UpdateOrderstatus(LoginPreferences.getActiveInstance(this).getToken(), getIntent().getStringExtra("booking_id"),orderstatus);
        call.enqueue(new Callback<UpdateOrderStatusResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<UpdateOrderStatusResponse> call, Response<UpdateOrderStatusResponse> response) {
                progressDialog.dismiss();
                try {
                    UpdateOrderStatusResponse resultFile = response.body();
                    Toast.makeText(BookingDetailsActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    if(resultFile.getCode() == 200)
                    {
                        booking_status=resultFile.getBooking_status();
                       // finish();
                        Intent i=new Intent(BookingDetailsActivity.this,MainActivity.class);
                        startActivity(i);
                        finish();
                    }
                    else
                        {
                        // Toast.makeText(LoginActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                       }
                } catch (Exception e) {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<UpdateOrderStatusResponse> call, Throwable t) {
                Toast.makeText(BookingDetailsActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }
}