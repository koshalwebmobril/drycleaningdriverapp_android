package com.wm.muggamudriver.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.wm.muggamudriver.ApiClient.RetrofitConnection;
import com.wm.muggamudriver.R;
import com.wm.muggamudriver.Model.resetpasswordmodel.ResetPasswordResponse;
import com.wm.muggamudriver.Utils.CommonMethod;
import com.wm.muggamudriver.Utils.ProgressD;
import com.wm.muggamudriver.network.ApiInterface;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;

public class ResetPasswordActivity extends AppCompatActivity {
    @BindView(R.id.back_arrow)
    ImageView backArrow;
    @BindView(R.id.imageview)
    ImageView imageview;
    @BindView(R.id.new_password)
    EditText newPassword;
    @BindView(R.id.confirm_password)
    EditText confirmPassword;
    @BindView(R.id.btn_resetpassword)
    Button btnResetpassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        ButterKnife.bind(this);
      //  init();


        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        btnResetpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.btn_resetpassword:
                        if (CommonMethod.isOnline(ResetPasswordActivity.this))
                        {
                            hideKeyboard((Button) v);
                            if (validation())
                            {
                                hitResetPasswordApi();
                            }
                        } else {
                            CommonMethod.showAlert(getString(R.string.check_internet), ResetPasswordActivity.this);
                        }
                        break;
                }
            }
        });
    }

    private boolean validation() {
        if (TextUtils.isEmpty(newPassword.getText().toString().trim())) {
            Toast.makeText(this, "Please Enter New Password", Toast.LENGTH_SHORT).show();
            return false;
        } else if (newPassword.getText().toString().trim().length() < 6) {
            Toast.makeText(this, "New Password should be at least 6 digit/characters long", Toast.LENGTH_LONG).show();
            return false;
        } else if (newPassword.getText().toString().trim().length() > 16) {
            Toast.makeText(this, "New Password Maximum. 16 characters or digits/characters would be allowed", Toast.LENGTH_LONG).show();
            return false;
        }
        else if (TextUtils.isEmpty(confirmPassword.getText().toString().trim())) {
            Toast.makeText(this, "Please enter Confirm Password", Toast.LENGTH_SHORT).show();
            return false;
        } else if (confirmPassword.getText().toString().trim().length() < 6) {
            Toast.makeText(this, "Confirm Password should be at least 6 digit/characters long", Toast.LENGTH_LONG).show();
            return false;
        } else if (confirmPassword.getText().toString().trim().length() > 16) {
            Toast.makeText(this, "Confirm Password Maximum. 16 characters or digits/characters would be allowed", Toast.LENGTH_LONG).show();
            return false;
        } else if (!confirmPassword.getText().toString().trim().equals(newPassword.getText().toString().trim())) {
            Toast.makeText(this, "New Password and Confirm Password Not Match!", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    public void hideKeyboard(View view) {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception ignored) {
        }
    }


    private void hitResetPasswordApi()
    {
        ProgressD progressDialog = ProgressD.show(ResetPasswordActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        ApiInterface service = RetrofitConnection.getInstance().createService();
        Call<ResetPasswordResponse> call = service.ResetPassword(getIntent().getStringExtra("email"),"4",newPassword.getText().toString().trim());
        call.enqueue(new Callback<ResetPasswordResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<ResetPasswordResponse> call, retrofit2.Response<ResetPasswordResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    ResetPasswordResponse resultFile = response.body();
                    Toast.makeText(ResetPasswordActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    if(resultFile.getCode() == 200)
                    {
                        Intent login = new Intent(ResetPasswordActivity.this, LoginActivity.class);
                        startActivity(login);
                    }
                    /*else if(resultFile.getCode()==400)
                    {
                        Intent login = new Intent(ResetPasswordActivity.this, OtpActivity.class);
                        login.putExtra("page_status","1");
                        startActivity(login);
                    }*/
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<ResetPasswordResponse> call, Throwable t)
            {
                Toast.makeText(ResetPasswordActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }




}