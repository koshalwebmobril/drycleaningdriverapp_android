package com.wm.muggamudriver.Activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.firebase.iid.FirebaseInstanceId;
import com.wm.muggamudriver.ApiClient.RetrofitConnection;
import com.wm.muggamudriver.R;
import com.wm.muggamudriver.Model.registerresponse.RegisterResponse;
import com.wm.muggamudriver.Utils.CommonMethod;
import com.wm.muggamudriver.Utils.GPSTracker;
import com.wm.muggamudriver.Utils.ProgressD;
import com.wm.muggamudriver.network.ApiInterface;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;

import static com.wm.muggamudriver.Utils.CommonMethod.isValidEmaillId;

public class RegisterActivity extends AppCompatActivity
{
    @BindView(R.id.back_arrow)
    ImageView backArrow;
    @BindView(R.id.imageview)
    ImageView imageview;
    @BindView(R.id.deltaRelative)
    RelativeLayout deltaRelative;
    @BindView(R.id.user_name)
    EditText userName;
    @BindView(R.id.email_address)
    EditText emailAddress;
    @BindView(R.id.mobile_number)
    EditText mobileNumber;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.btn_signup)
    Button btnSignup;
    @BindView(R.id.sign_in)
    TextView signIn;

    String notification_token;
    GPSTracker gpsTracker;
    private static final int PERMISSION_REQ_CODE = 1<<2;
    private String[] PERMISSIONS = {
            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION,};
    String user_lat,user_lng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
      //  init();
    }
    @OnClick({R.id.back_arrow, R.id.btn_signup,R.id.sign_in})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_arrow:
                finish();
                break;
            case R.id.btn_signup:
                if (CommonMethod.isOnline(this))
                {
                    notification_token = FirebaseInstanceId.getInstance().getToken();
                    hideKeyboard((Button) view);
                    checkPermission();

                }
                else
                {
                    CommonMethod.showAlert(getString(R.string.check_internet), this);
                }
                break;

            case R.id.sign_in:
                Intent signin = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(signin);
                break;
        }
    }

    public void hideKeyboard(View view)
    {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception ignored) {
        }
    }

    private void checkPermission()
    {
        boolean granted = true;
        for (String per : PERMISSIONS) {
            if (!permissionGranted(per))
            {
                granted = false;
                break;
            }
        }
        if (granted)
        {
            checkGPS();
        }
        else
        {
            requestPermissions();
        }
    }
    private boolean permissionGranted(String permission)
    {
        return ContextCompat.checkSelfPermission(
                this, permission) == PackageManager.PERMISSION_GRANTED;
    }
    private void requestPermissions() {
        ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_REQ_CODE);
    }
    private void toastNeedPermissions() {
        Toast.makeText(this, "You Need Accept This Location Permission", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQ_CODE) {
            boolean granted = true;
            for (int result : grantResults)
            {
                granted = (result == PackageManager.PERMISSION_GRANTED);
                if (!granted) break;
            }
            if (granted)
            { }
            else
            {
                toastNeedPermissions();
            }
        }
    }
    private void checkGPS()
    {
        final LocationManager manager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER))
        {
            Toast.makeText(RegisterActivity.this, "Please Turn GPS on to continue..", Toast.LENGTH_LONG).show();
        }
        else
        {
            if(validation())
            {
                gpsTracker=new GPSTracker(this);
                user_lat=String.valueOf(gpsTracker.getLatitude());
                user_lng=String.valueOf(gpsTracker.getLongitude());
                RegisterUserApi();
            }
        }
    }

    private boolean validation()
    {
        if(TextUtils.isEmpty(userName.getText().toString().trim()))
        {
            Toast.makeText(this, "Please enter company name", Toast.LENGTH_SHORT).show();
            return false;
        }

        else if (TextUtils.isEmpty(emailAddress.getText().toString().trim())) {
            Toast.makeText(this, "Please enter Email id", Toast.LENGTH_SHORT).show();
            return false;
        }

        else if (!isValidEmaillId(emailAddress.getText().toString().trim()))
        {
            Toast.makeText(this, "Please enter valid Email id", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(TextUtils.isEmpty(mobileNumber.getText().toString().trim()))
        {
            Toast.makeText(this, "Please enter mobile number", Toast.LENGTH_SHORT).show();
            return false;
        }

        else if (mobileNumber.getText().toString().trim().length() < 7)
        {
            Toast.makeText(this, "Mobile number should be min 7 digit", Toast.LENGTH_LONG).show();
            return false;
        }

        else if (mobileNumber.getText().toString().trim().length() > 15)
        {
            Toast.makeText(this, "Mobile number should be max 15 digit", Toast.LENGTH_LONG).show();
            return false;
        }

        else if (TextUtils.isEmpty(password.getText().toString().trim()))
        {
            Toast.makeText(this, "Please enter password", Toast.LENGTH_SHORT).show();
            return false;
        }


        else if (password.getText().toString().trim().length() < 6){
            Toast.makeText(this, "Password should be at least 6 digit/characters long", Toast.LENGTH_LONG).show();
            return false;
        }
        else if (password.getText().toString().trim().length() > 16)
        {
            Toast.makeText(this, "Password Maximum. 16 characters or digits/characters would be allowed", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    private void RegisterUserApi()
    {
        final ProgressD progressDialog = ProgressD.show(RegisterActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        ApiInterface service = RetrofitConnection.getInstance().createService();

        Call<RegisterResponse> call = service.RegisterUser(userName.getText().toString().trim(),emailAddress.getText().toString().trim(),
                mobileNumber.getText().toString().trim(),"4",password.getText().toString().trim(),
                "2",notification_token,user_lat,user_lng);
        call.enqueue(new Callback<RegisterResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<RegisterResponse> call, retrofit2.Response<RegisterResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    RegisterResponse resultFile = response.body();
                    Toast.makeText(RegisterActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    if (resultFile.getCode() == 200)
                    {
                        Intent i=new Intent(RegisterActivity.this, OtpActivity.class);
                        i.putExtra("page_status","1");
                        i.putExtra("email",resultFile.getRegisterModel().getEmail());
                        startActivity(i);
                    }
                    else if(resultFile.getCode() == 401)
                    {

                    }
                    else
                    {
                        //  Toast.makeText(RegisterActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Register Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t)
            {
                Toast.makeText(RegisterActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        /*userName.setText("");
        emailAddress.setText("");
        mobileNumber.setText("");
        password.setText("");*/
    }
}