package com.wm.muggamudriver.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.wm.muggamudriver.Adapter.NotificationAdapter;
import com.wm.muggamudriver.Adapter.RejectOrderAdapter;
import com.wm.muggamudriver.ApiClient.RetrofitConnection;
import com.wm.muggamudriver.Interface.NotificationClick;
import com.wm.muggamudriver.Model.orderrejectmodel.OrderRejectModel;
import com.wm.muggamudriver.Model.notificationmodel.NotificationModel;
import com.wm.muggamudriver.R;
import com.wm.muggamudriver.Model.acceptbookingmodel.AcceptBookingResponse;
import com.wm.muggamudriver.Model.cancelreasonmodel.CancelReasonResponse;
import com.wm.muggamudriver.Model.notificationmodel.NotificationResponse;
import com.wm.muggamudriver.Model.popupdatamodel.PopUpDataResponse;
import com.wm.muggamudriver.Model.orderrejectmodel.RejectBookingResponse;
import com.wm.muggamudriver.Sharedpreference.LoginPreferences;
import com.wm.muggamudriver.Utils.ProgressD;
import com.wm.muggamudriver.network.ApiInterface;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;

public class NotificationActivity extends AppCompatActivity implements NotificationClick, View.OnClickListener {

    RecyclerView recyclerview_notification;
    NotificationAdapter notificationAdapter;
    ImageView back;
    NotificationClick notificationClick;
    List<NotificationModel> notificationModelList;
    ImageView user_image;
    private TextView txtusername,txtdelivery_date,pickuplocation,deliverylocation;
    ProgressD progressDialog;
     Dialog dialog_accept;
    private Dialog dialog_reject;
    List<OrderRejectModel> cancelReasonList;
    RadioButton rbn;
    CancelReasonResponse resultFile;
    private OrderRejectModel cancelreasonmodel;
    private RadioGroup rgp;
    int cancelreasonid;
    private Button btn_submit;
    private ArrayList<Integer> idslist;
    private int position;
    private RejectOrderAdapter rejectorderadapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        init();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        getNotificationApi();
    }
    public void init()
    {
        notificationClick= (NotificationClick) this;
        recyclerview_notification=findViewById(R.id.recyclerview_notification);
        back=findViewById(R.id.back);
    }


    public void getNotificationApi()
    {
        final ProgressD progressDialog = ProgressD.show(NotificationActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        ApiInterface service = RetrofitConnection.getInstance().createService();
        Call<NotificationResponse> call = service.getNotification(LoginPreferences.getActiveInstance(NotificationActivity.this).getToken());
        call.enqueue(new Callback<NotificationResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<NotificationResponse> call, retrofit2.Response<NotificationResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    NotificationResponse resultFile = response.body();
                    if(resultFile.getCode() == 200)
                    {
                        notificationModelList=resultFile.getNotificationList();
                        LinearLayoutManager mLayoutManager = new LinearLayoutManager(NotificationActivity.this,RecyclerView.VERTICAL,false);
                        recyclerview_notification.setLayoutManager(mLayoutManager);
                        notificationAdapter = new NotificationAdapter(NotificationActivity.this,notificationModelList,notificationClick);
                        recyclerview_notification.setAdapter(notificationAdapter);
                    }
                    else if(resultFile.getCode()== 401)
                    {
                        Toast.makeText(NotificationActivity.this, "Unauthorized", Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<NotificationResponse> call, Throwable t)
            {
                Toast.makeText(NotificationActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    @Override
    public void NotificationPosition(int position,int bookedid)
    {
        progressDialog = ProgressD.show(NotificationActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        hitAcceptRejectDailogApi(bookedid);
    }


    public void hitAcceptRejectDailogApi(int booked_id)
    {
        ApiInterface service = RetrofitConnection.getInstance().createService();
        Call<PopUpDataResponse> call = service.popupData(LoginPreferences.getActiveInstance(NotificationActivity.this).getToken(),String.valueOf(booked_id));
        call.enqueue(new Callback<PopUpDataResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<PopUpDataResponse> call, retrofit2.Response<PopUpDataResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    PopUpDataResponse resultFile = response.body();
                   // Toast.makeText(NotificationActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    if(resultFile.getCode() == 200)
                    {
                        dialog_accept = new Dialog(NotificationActivity.this);
                        dialog_accept.setCancelable(true);
                        dialog_accept.setContentView(R.layout.item_alert_dailog_new_request);
                        Objects.requireNonNull(dialog_accept.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        dialog_accept.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                        user_image = dialog_accept.findViewById(R.id.user_image);
                        txtusername= dialog_accept.findViewById(R.id.txtusername);
                        txtdelivery_date = dialog_accept.findViewById(R.id.txtdelivery_date);
                        pickuplocation = dialog_accept.findViewById(R.id.txtpickuplocation);
                        deliverylocation = dialog_accept.findViewById(R.id.txtdroplocation);
                        Button accept= dialog_accept.findViewById(R.id.btn_accept);
                        Button reject= dialog_accept.findViewById(R.id.btn_reject);

                        Glide.with(NotificationActivity.this)
                                .load(resultFile.getPopupDetails().getUser_Profile()).apply(new RequestOptions().placeholder(R.drawable.user_profile_service).error(R.drawable.user_profile_service))
                                .into(user_image);

                        txtusername.setText(resultFile.getPopupDetails().getUsername());
                        txtdelivery_date.setText(resultFile.getPopupDetails().getDeliveryDate());
                        pickuplocation.setText(resultFile.getPopupDetails().getPickupLocation());
                        deliverylocation.setText(resultFile.getPopupDetails().getDropLocation());
                        dialog_accept.show();


                        accept.setOnClickListener(new View.OnClickListener()
                            {
                               @Override
                                public void onClick(View v)
                                   {
                                       hitAcceptApi(booked_id);
                                   }
                              });

                        reject.setOnClickListener(new View.OnClickListener()
                        {
                            @Override
                            public void onClick(View v)
                            {
                                hitCancelReasonList(booked_id);
                            }
                        });
                    }
                    else if(resultFile.getCode()== 400)
                    {
                        Toast.makeText(NotificationActivity.this,resultFile.getMessage() , Toast.LENGTH_SHORT).show();
                    }
                    else if(resultFile.getCode()== 401)
                    {
                        Toast.makeText(NotificationActivity.this,resultFile.getMessage() , Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<PopUpDataResponse> call, Throwable t)
            {
                Toast.makeText(NotificationActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    public void hitAcceptApi(int booking_id)
    {
        final ProgressD progressDialog = ProgressD.show(NotificationActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        ApiInterface service = RetrofitConnection.getInstance().createService();
        Call<AcceptBookingResponse> call = service.acceptBooking(LoginPreferences.getActiveInstance(NotificationActivity.this).getToken(),String.valueOf(booking_id));
        call.enqueue(new Callback<AcceptBookingResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<AcceptBookingResponse> call, retrofit2.Response<AcceptBookingResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    AcceptBookingResponse resultFile = response.body();
                    Toast.makeText(NotificationActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    if(resultFile.getCode() == 200)
                    {
                        dialog_accept.dismiss();
                        Intent i=new Intent(NotificationActivity.this,MainActivity.class);
                        startActivity(i);
                    }
                    else if(resultFile.getCode()== 401)
                    {
                      //  Toast.makeText(NotificationActivity.this, "Unauthorized", Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<AcceptBookingResponse> call, Throwable t)
            {
                Toast.makeText(NotificationActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    public void hitCancelReasonList(int booking_id)
    {
        final ProgressD progressDialog = ProgressD.show(NotificationActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        ApiInterface service = RetrofitConnection.getInstance().createService();
        Call<CancelReasonResponse> call = service.GetReason(LoginPreferences.getActiveInstance(NotificationActivity.this).getToken());
        call.enqueue(new Callback<CancelReasonResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<CancelReasonResponse> call, retrofit2.Response<CancelReasonResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    resultFile = response.body();
                    //  Toast.makeText(NotificationActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    if(resultFile.getCode() == 200)
                    {
                        dialog_accept.dismiss();
                        cancelReasonList=resultFile.getReasonList();
                        openRejectReasonDialog(cancelReasonList,booking_id);

                    }
                    else if(resultFile.getCode()== 401)
                    {
                        Toast.makeText(NotificationActivity.this, "Unauthorized", Toast.LENGTH_SHORT).show();
                    }
                    else if(resultFile.getCode()== 400)
                    {
                        Toast.makeText(NotificationActivity.this,resultFile.getMessage() , Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<CancelReasonResponse> call, Throwable t)
            {
                Toast.makeText(NotificationActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    public void openRejectReasonDialog(List cancelReasonList,int booking_id)
    {
        position=0;
        dialog_reject = new Dialog(NotificationActivity.this);
        dialog_reject.setCancelable(true);
        dialog_reject.setContentView(R.layout.item_alert_dailog_cancellation_request);
        Objects.requireNonNull(dialog_reject.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog_reject.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
         btn_submit=dialog_reject.findViewById(R.id.btn_submit);
         RecyclerView rejectorderrecyclerview=dialog_reject.findViewById(R.id.recycler_cancel_order);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(NotificationActivity.this,RecyclerView.VERTICAL,false);
        rejectorderrecyclerview.setLayoutManager(mLayoutManager);
        rejectorderadapter = new RejectOrderAdapter(NotificationActivity.this,cancelReasonList);
        rejectorderrecyclerview.setAdapter(rejectorderadapter );


        /* rgp = dialog_reject.findViewById(R.id.radio_group);
         rgp.setOrientation(LinearLayout.VERTICAL);
        idslist=new ArrayList<>();

        for(int i=0; i < cancelReasonList.size(); i++)
        {
            cancelreasonmodel = (OrderRejectModel) cancelReasonList.get(i);
            rbn = new RadioButton(NotificationActivity.this);
            idslist.add(cancelreasonmodel.getId());
            rbn.setText(cancelreasonmodel.getDescription());
            rbn.setId(View.generateViewId());
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1f);
            rbn.setLayoutParams(params);
            rgp.addView(rbn);
            rbn.setOnClickListener(NotificationActivity.this);
        }*/
        /*rgp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                Log.d("ids",""+checkedId);
                switch(checkedId)
                {

                }
            }
        });*/
        btn_submit.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //  Log.d("cancelid",LoginPreferences.getActiveInstance(NotificationActivity.this).getCancelId());
              hitSubmitRejectReasonApi(String.valueOf(booking_id));
                /*if(rgp.getCheckedRadioButtonId() == -1)
                {
                    Toast.makeText(NotificationActivity.this, "Please select cancel reason", Toast.LENGTH_SHORT).show();
                }
                else
                {

                }*/
            }
        });
        dialog_reject.show();
    }
    @Override
    public void onClick(View v)
    {
        /*try
        {
            position=rgp.getCheckedRadioButtonId();
            cancelreasonid = idslist.get(position-1).intValue();
            Log.d("tag", " Name " + ((RadioButton)v).getText() +"Idis"+cancelreasonid);
        }
        catch (Exception e)
        { }*/
    }
    public void hitSubmitRejectReasonApi(String booking_id)
    {
        ProgressD progressDialog = ProgressD.show(NotificationActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        ApiInterface service = RetrofitConnection.getInstance().createService();
        Call<RejectBookingResponse> call = service.rejectBooking(LoginPreferences.getActiveInstance(NotificationActivity.this).getToken(),booking_id,LoginPreferences.getActiveInstance(this).getCancelId());
        call.enqueue(new Callback<RejectBookingResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<RejectBookingResponse> call, retrofit2.Response<RejectBookingResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                      RejectBookingResponse resulfile = response.body();
                      Toast.makeText(NotificationActivity.this, resulfile.getMessage(), Toast.LENGTH_SHORT).show();
                    if(resultFile.getCode() == 200)
                    {
                        dialog_reject.dismiss();
                        Intent i=new Intent(NotificationActivity.this,MainActivity.class);
                        startActivity(i);
                    }
                    else if(resultFile.getCode()== 401)
                    {
                       // Toast.makeText(NotificationActivity.this, "Unauthorized", Toast.LENGTH_SHORT).show();
                    }
                    else if(resultFile.getCode()== 400)
                    {
                        dialog_reject.dismiss();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<RejectBookingResponse> call, Throwable t)
            {
                Toast.makeText(NotificationActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }



}