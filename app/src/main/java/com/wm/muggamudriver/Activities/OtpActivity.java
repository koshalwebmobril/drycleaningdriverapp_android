package com.wm.muggamudriver.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.wm.muggamudriver.ApiClient.RetrofitConnection;
import com.wm.muggamudriver.Model.forgotpasswordmodel.ForgotPasswordResponse;
import com.wm.muggamudriver.Model.otpverifiymodel.OtpVerifiyModel;
import com.wm.muggamudriver.R;
import com.wm.muggamudriver.Model.otpverifiymodel.OtpVerifiyResponse;
import com.wm.muggamudriver.Sharedpreference.LoginPreferences;
import com.wm.muggamudriver.Utils.CommonMethod;
import com.wm.muggamudriver.Utils.ProgressD;
import com.wm.muggamudriver.network.ApiInterface;

import in.aabhasjindal.otptextview.OTPListener;
import in.aabhasjindal.otptextview.OtpTextView;
import retrofit2.Call;
import retrofit2.Callback;

public class OtpActivity extends AppCompatActivity
{
    Button btn_confirm;
    private OtpTextView otpTextView;
    String email;
    String otpuser;
    ImageView back_arrow;
    TextView resend_otp;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        init();
        btn_confirm.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(CommonMethod.isOnline(OtpActivity.this))
                  {
                    otpuser=otpTextView.getOTP();
                    if(otpuser.length() < 4)
                    {
                        Toast.makeText(OtpActivity.this, "Please enter OTP first", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    else
                    {
                        OtpVerifyApi();
                    }
                    hideKeyboard((Button)v);
                }
                else
                {
                    CommonMethod.showAlert(getString(R.string.check_internet), OtpActivity.this);
                }
            }
        });

        otpTextView.setOtpListener(new OTPListener()
        {
            @Override
            public void onInteractionListener()
            {
                // fired when user types something in the Otpbox
            }
            @Override
            public void onOTPComplete(String otp)
            {
                otpuser =otp.toString().trim();
                if(otpuser.length()<4)
                {

                }
                else
                {
                    hideKeyboard(otpTextView);
                }
            }
        });

        back_arrow.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
               /* Intent i=new Intent(OtpActivity.this,ForgotPasswordActivity.class);
                startActivity(i);
                finish();*/
            }
        });

        resend_otp.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                 ResendOtpApi();
            }
        });
    }

    public void init()
    {
        email=getIntent().getStringExtra("email");
        btn_confirm=findViewById(R.id.btn_confirm);
        otpTextView=findViewById(R.id.otp_view);
        back_arrow=findViewById(R.id.back_arrow);
        resend_otp=findViewById(R.id.resend_otp);
    }

    private void OtpVerifyApi()
    {
        final ProgressD progressDialog = ProgressD.show(OtpActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        ApiInterface service = RetrofitConnection.getInstance().createService();
        Call<OtpVerifiyResponse> call = service.OtpVerifiey(getIntent().getStringExtra("email"),otpuser);
        call.enqueue(new Callback<OtpVerifiyResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<OtpVerifiyResponse> call, retrofit2.Response<OtpVerifiyResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    OtpVerifiyResponse resultFile = response.body();
                    Toast.makeText(OtpActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    if(resultFile.getCode() == 200)
                    {
                        if(getIntent().getStringExtra("page_status").equals("1"))
                        {
                            OtpVerifiyModel verifyOtpModel=resultFile.getOtpVerifiyModel();
                            LoginPreferences.getActiveInstance(OtpActivity.this).setToken("Bearer "+ verifyOtpModel.getToken());
                            LoginPreferences.getActiveInstance(OtpActivity.this).setUserName(verifyOtpModel.getName());
                            LoginPreferences.getActiveInstance(OtpActivity.this).setUserEmail(verifyOtpModel.getEmail());
                            LoginPreferences.getActiveInstance(OtpActivity.this).setUserProfile(verifyOtpModel.getProfileImagePath());
                            Intent i=new Intent(OtpActivity.this,MainActivity.class);
                            startActivity(i);
                        }
                        else
                        {
                            OtpVerifiyModel verifyOtpModel=resultFile.getOtpVerifiyModel();
                            LoginPreferences.getActiveInstance(OtpActivity.this).setToken("Bearer "+ verifyOtpModel.getToken());
                            LoginPreferences.getActiveInstance(OtpActivity.this).setUserName(verifyOtpModel.getName());
                            LoginPreferences.getActiveInstance(OtpActivity.this).setUserEmail(verifyOtpModel.getEmail());
                            LoginPreferences.getActiveInstance(OtpActivity.this).setUserProfile(verifyOtpModel.getProfileImagePath());
                            Intent i=new Intent(OtpActivity.this,ResetPasswordActivity.class);
                            i.putExtra("email",email);
                            startActivity(i);
                        }
                    }
                    else
                    { }
                }
                catch (Exception e)
                {
                    Log.e("Register Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<OtpVerifiyResponse> call, Throwable t)
            {
                Toast.makeText(OtpActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    public void hideKeyboard(View view) {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch(Exception ignored) {
        }
    }

    private void ResendOtpApi()
    {
        final ProgressD progressDialog = ProgressD.show(OtpActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        ApiInterface service = RetrofitConnection.getInstance().createService();
        Call<ForgotPasswordResponse> call = service.Resendotp(email, "4");
        call.enqueue(new Callback<ForgotPasswordResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<ForgotPasswordResponse> call, retrofit2.Response<ForgotPasswordResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    ForgotPasswordResponse resultFile = response.body();
                    Toast.makeText(OtpActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    if (resultFile.getCode() == 200)
                    {
                       /* Intent i=new Intent(OtpActivity.this,OtpActivity.class);
                        i.putExtra("email",email.getText().toString().trim());
                        i.putExtra("page_status","2");
                        startActivity(i);*/
                    }
                    else
                    { }
                }
                catch (Exception e)
                {
                    Log.e("Forgot Password  Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<ForgotPasswordResponse> call, Throwable t)
            {
                Toast.makeText(OtpActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }
}