package com.wm.muggamudriver.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.bumptech.glide.Glide;
import com.wm.muggamudriver.Fragments.HomeFragment;
import com.wm.muggamudriver.R;
import com.wm.muggamudriver.Sharedpreference.LoginPreferences;
import com.wm.muggamudriver.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity implements View.OnClickListener
{
    ActivityMainBinding binding;
    private FragmentTransaction ft;
    private Fragment currentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        setSupportActionBar(binding.layoutContent.toolbar.toolbarMain);
        init();
        loadHome("0");
    }

    private void init()
    {
        binding.drawerMenuItems.firstName.setText(LoginPreferences.getActiveInstance(MainActivity.this).getUserName());
        binding.drawerMenuItems.gmail.setText(LoginPreferences.getActiveInstance(MainActivity.this).getUserEmail());
        Glide.with(MainActivity.this).load(LoginPreferences.getActiveInstance(MainActivity.this).getUserProfile()).error(R.drawable.profile_white).placeholder(R.drawable.profile_white)
                .into(binding.drawerMenuItems.profileimg);

        binding.layoutContent.toolbar.imgMenu.setOnClickListener(MainActivity.this);
        binding.drawerMenuItems.txtProfile.setOnClickListener(this);
        binding.drawerMenuItems.txtChangePassword.setOnClickListener(this);
        binding.drawerMenuItems.txtContactUs.setOnClickListener(this);
        binding.drawerMenuItems.txtprivacy.setOnClickListener(this);
        binding.drawerMenuItems.txttermcodition.setOnClickListener(this);
        binding.drawerMenuItems.txtyourlocation.setOnClickListener(this);
        binding.drawerMenuItems.crossbutton.setOnClickListener(this);
        binding.drawerMenuItems.logout.setOnClickListener(this);
        binding.layoutContent.toolbar.imgnotification.setOnClickListener(this);
    }

    public void toolbarHome()
    {
        binding.layoutContent.toolbar.lnMain.setVisibility(View.VISIBLE);
        //  binding.layoutContent.toolbar.lnOtherTool.setVisibility(View.GONE);
    }


    private void loadHome(String anim) {
        ft = getSupportFragmentManager().beginTransaction();
        Bundle bundle = new Bundle();
        currentFragment = new HomeFragment();
        currentFragment.setArguments(bundle);
        if (anim.equals("1")) {
            ft.setCustomAnimations(R.anim.left_in, R.anim.right_out);
        }
        ft.replace(R.id.frameMain, currentFragment);
        ft.commit();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.imgMenu) {
            if (!binding.drawerLayout.isDrawerOpen(GravityCompat.START))
                binding.drawerLayout.openDrawer(GravityCompat.START);
            else binding.drawerLayout.closeDrawer(GravityCompat.START);
        }

        if (v.getId() == R.id.txt_profile) {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
            Intent i = new Intent(MainActivity.this, ProfileActivity.class);
            startActivity(i);
        }

        if (v.getId() == R.id.txt_change_password) {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
            Intent i = new Intent(MainActivity.this, ChangePasswordActivity.class);
            startActivity(i);
        }

        if (v.getId() == R.id.txt_contact_us) {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
            Intent i = new Intent(MainActivity.this, ContactUsActivity.class);
            startActivity(i);
        }

        if (v.getId() == R.id.txtprivacy) {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
            Intent i = new Intent(MainActivity.this, PrivacyPolicyContactUsActivity.class);
            i.putExtra("page_title", "Privacy Policy");
            i.putExtra("page_status","1");
            startActivity(i);
        }

        if (v.getId() == R.id.txttermcodition) {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
            Intent i = new Intent(MainActivity.this, PrivacyPolicyContactUsActivity.class);
            i.putExtra("page_title", "Terms & Conditions");
            i.putExtra("page_status","2");
            startActivity(i);
        }

        if (v.getId() == R.id.txtyourlocation) {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
            Intent i = new Intent(MainActivity.this, YourLocationActivity.class);
            startActivity(i);
        }

        /*if (v.getId() == R.id.txtAddlocation) {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
            Intent i = new Intent(MainActivity.this, AddLocation.class);
            startActivity(i);
        }*/

        if (v.getId() == R.id.imgnotification) {
            Intent i = new Intent(MainActivity.this, NotificationActivity.class);
            startActivity(i);
        }

        if (v.getId() == R.id.crossbutton) {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
        }

        if (v.getId() == R.id.logout)
        {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle(getString(R.string.really_exit))
                    .setMessage(getString(R.string.are_sure_logout))
                    .setPositiveButton(getString(R.string.yes), (dialog, which) ->
                    {
                        Intent i=new Intent(MainActivity.this,LoginActivity.class);
                        LoginPreferences.deleteAllPreference();
                        startActivity(i);
                    })
                    .setNegativeButton(getString(R.string.no), null).show();
        }
    }

    @Override
    public void onBackPressed()
    {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
        }

        else if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else if (getSupportFragmentManager().getBackStackEntryCount() < 0) {
        }


        else if (getSupportFragmentManager().getBackStackEntryCount() == 0)
        {
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle(getString(R.string.really_exit))
                    .setMessage(getString(R.string.are_sure_exit))
                    .setPositiveButton(getString(R.string.yes), (dialog, which) -> MainActivity.this.finishAffinity())
                    .setNegativeButton(getString(R.string.no), null).show();
        }
    }



    /*public void showDialog()
    {
        final Dialog dialog = new Dialog(this);
        //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.item_alert_dailog_cancellation_request);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        *//* TextView btnOk = dialog.findViewById(R.id.btn_ok);
        TextView tv_message = dialog.findViewById(R.id.tv_message);
        tv_message.setText(message);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
               // startIntent(EnterOTPReset.class);
            }
        });*//*
        dialog.show();
    }*/


    @Override
    protected void onResume()
    {
        super.onResume();
        binding.drawerMenuItems.firstName.setText(LoginPreferences.getActiveInstance(MainActivity.this).getUserName());
        binding.drawerMenuItems.gmail.setText(LoginPreferences.getActiveInstance(MainActivity.this).getUserEmail());
        Glide.with(MainActivity.this).load(LoginPreferences.getActiveInstance(MainActivity.this).getUserProfile()).error(R.drawable.profile_white).placeholder(R.drawable.profile_white)
                .into(binding.drawerMenuItems.profileimg);
    }
}






