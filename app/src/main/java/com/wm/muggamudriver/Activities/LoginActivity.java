package com.wm.muggamudriver.Activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.firebase.iid.FirebaseInstanceId;
import com.wm.muggamudriver.ApiClient.RetrofitConnection;
import com.wm.muggamudriver.R;
import com.wm.muggamudriver.Model.loginmodel.Login1Response;
import com.wm.muggamudriver.Sharedpreference.LoginPreferences;
import com.wm.muggamudriver.Utils.CommonMethod;
import com.wm.muggamudriver.Utils.GPSTracker;
import com.wm.muggamudriver.Utils.ProgressD;
import com.wm.muggamudriver.network.ApiInterface;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;

import static android.content.ContentValues.TAG;
import static com.wm.muggamudriver.Utils.CommonMethod.isValidEmaillId;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener
{
    TextView forgot_password, signup;
    Button btn_login;
    String notification_token;
    @BindView(R.id.imageview)
    ImageView imageview;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.forgot_password)
    TextView forgotPassword;
    @BindView(R.id.btn_login)
    Button btnLogin;
    GPSTracker gpsTracker;
    private static final int PERMISSION_REQ_CODE = 1<<2;
    private String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION,};
   String user_lat,user_lng;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        init();
    }

    public void init() {
        forgot_password = findViewById(R.id.forgot_password);
        signup = findViewById(R.id.signup);
        btn_login = findViewById(R.id.btn_login);

        forgot_password.setOnClickListener(this);
        signup.setOnClickListener(this);
        btn_login.setOnClickListener(this);
    }

    public void hideKeyboard(View view) {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception ignored) {
        }
    }


    private boolean validation() {
        if (TextUtils.isEmpty(email.getText().toString().trim())) {
            Toast.makeText(this, "Please enter Email id", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!isValidEmaillId(email.getText().toString().trim())) {
            Toast.makeText(this, "Please enter valid Email id", Toast.LENGTH_SHORT).show();
            return false;
        } else if (TextUtils.isEmpty(password.getText().toString().trim())) {
            Toast.makeText(this, "Please enter password", Toast.LENGTH_SHORT).show();
            return false;
        } else if (password.getText().toString().trim().length() < 6) {
            Toast.makeText(this, "Password should be at least 6 digit/characters long", Toast.LENGTH_LONG).show();
            return false;
        } else if (password.getText().toString().trim().length() > 16) {
            Toast.makeText(this, "Password Maximum. 16 characters or digits/characters would be allowed", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    @OnClick({R.id.btn_login,R.id.forgot_password,R.id.signup})
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.forgot_password:
                Intent i = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                startActivity(i);
                break;

            case R.id.signup:
                Intent sign = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(sign);
                break;

            case R.id.btn_login:
                if (CommonMethod.isOnline(this))
                {
                    notification_token = FirebaseInstanceId.getInstance().getToken();
                    Log.d(TAG, notification_token);
                    hideKeyboard((Button) v);
                    checkPermission();
                } else {
                    CommonMethod.showAlert(getString(R.string.check_internet), this);
                }
                break;
        }
    }

    private void checkPermission()
    {
        boolean granted = true;
        for (String per : PERMISSIONS) {
            if (!permissionGranted(per))
            {
                granted = false;
                break;
            }
        }
        if (granted)
        {
            checkGPS();
        }
        else
        {
            requestPermissions();
        }
    }
    private boolean permissionGranted(String permission)
    {
        return ContextCompat.checkSelfPermission(
                this, permission) == PackageManager.PERMISSION_GRANTED;
    }
    private void requestPermissions() {
        ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_REQ_CODE);
    }
    private void toastNeedPermissions() {
        Toast.makeText(this, "You Need Accept This Location Permission", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQ_CODE) {
            boolean granted = true;
            for (int result : grantResults)
            {
                granted = (result == PackageManager.PERMISSION_GRANTED);
                if (!granted) break;
            }
            if (granted)
            { }
            else
            {
                toastNeedPermissions();
            }
        }
    }
    private void checkGPS()
    {
        final LocationManager manager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER))
        {
            Toast.makeText(LoginActivity.this, "Please Turn GPS on to continue..", Toast.LENGTH_LONG).show();
        }
        else
        {
            if(validation())
            {
                gpsTracker=new GPSTracker(this);
                user_lat=String.valueOf(gpsTracker.getLatitude());
                user_lng=String.valueOf(gpsTracker.getLongitude());
                hitLoginUserApi();
            }
        }
    }

    private void hitLoginUserApi()
    {
        ProgressD progressDialog = ProgressD.show(LoginActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        ApiInterface service = RetrofitConnection.getInstance().createService();
        Call<Login1Response> call = service.LoginUser(email.getText().toString().trim(),password.getText().toString().trim(),"4","2",notification_token,user_lat,user_lng);
        call.enqueue(new Callback<Login1Response>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<Login1Response> call, retrofit2.Response<Login1Response> response)
            {
                progressDialog.dismiss();
                try
                {
                    Login1Response resultFile = response.body();
                    Toast.makeText(LoginActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    if(resultFile.getCode() == 200)
                    {
                        Intent login = new Intent(LoginActivity.this, MainActivity.class);
                        LoginPreferences.getActiveInstance(LoginActivity.this).setUserName(resultFile.getLoginModel().getName());
                        LoginPreferences.getActiveInstance(LoginActivity.this).setUserEmail(resultFile.getLoginModel().getEmail());
                        LoginPreferences.getActiveInstance(LoginActivity.this).setToken("Bearer "+ resultFile.getLoginModel().getToken());
                        startActivity(login);
                    }
                    else if(resultFile.getCode()==400)
                    {
                        Intent login = new Intent(LoginActivity.this, OtpActivity.class);
                        login.putExtra("page_status","1");
                        login.putExtra("email",email.getText().toString());
                        startActivity(login);
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<Login1Response> call, Throwable t)
            {
                Toast.makeText(LoginActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    @Override
    protected void onResume()
    {
        super.onResume();
      /*  email.setText("");
        password.setText("");*/
    }

    @Override
    public void onBackPressed() {
        finishAffinity();
        finish();
    }
}