package com.wm.muggamudriver.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.wm.muggamudriver.R;

public class FindLocationActivity extends AppCompatActivity
{

    RelativeLayout relative_use_current_location;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_location);
        init();

        relative_use_current_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(FindLocationActivity.this,MainActivity.class);
                startActivity(i);
            }
        });

    }

    public void init()
    {
        relative_use_current_location=findViewById(R.id.relative_use_current_location);
    }

}