package com.wm.muggamudriver.Activities;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.wm.muggamudriver.R;
import com.wm.muggamudriver.Utils.GPSTracker;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class YourLocationActivity extends AppCompatActivity implements OnMapReadyCallback,GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.title)
    TextView title;


    @BindView(R.id.txtaddress)
    TextView txtaddress;
    @BindView(R.id.relative_toolbar)
    RelativeLayout relativeToolbar;

    @BindView(R.id.relative_details)
    RelativeLayout relativeDetails;
    SupportMapFragment mapFragment;
    ArrayList<LatLng> mMarkerPoints;
    GPSTracker gpsTracker;
    private static final int PERMISSION_REQ_CODE = 1<<2;
    private String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION,};

    private GoogleMap mMap;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    private double current_location_longitude;
    private double current_location_latitude;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    LocationRequest mLocationRequest;
    private LatLng latLng_origin;
    Marker mCurrLocationMarker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_your_location);
        ButterKnife.bind(this);
        checkPermission();
        mapFragment = (SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mMarkerPoints = new ArrayList<>();
    }
    @OnClick(R.id.back)
    public void onClick()
    {
        finish();
    }

    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mMap = googleMap;
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(YourLocationActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                //Location Permission already granted
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
            else
            {
                checkPermission();
            }
        }
        else
            {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
          }
    }

    private void checkPermission()
    {
        boolean granted = true;
        for (String per : PERMISSIONS) {
            if (!permissionGranted(per))
            {
                granted = false;
                break;
            }
        }
        if (granted)
        {
            checkGPS();
        }
        else
        {
            requestPermissions();
        }
    }
    private boolean permissionGranted(String permission)
    {
        return ContextCompat.checkSelfPermission(
                this, permission) == PackageManager.PERMISSION_GRANTED;
    }
    private void requestPermissions() {
        ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_REQ_CODE);
    }
    private void toastNeedPermissions() {
        Toast.makeText(this, "You Need Accept This Location Permission", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQ_CODE) {
            boolean granted = true;
            for (int result : grantResults)
            {
                granted = (result == PackageManager.PERMISSION_GRANTED);
                if (!granted) break;
            }
            if (granted)
            { }
            else
            {
                toastNeedPermissions();
            }
        }
    }
    private void checkGPS()
    {
        final LocationManager manager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER))
        {
            Toast.makeText(YourLocationActivity.this, "Please Turn GPS on to continue..", Toast.LENGTH_LONG).show();
        }
        else
        {
            /*gpsTracker=new GPSTracker(this);
            user_lat=String.valueOf(gpsTracker.getLatitude());
            user_lng=String.valueOf(gpsTracker.getLongitude());
            txtaddress.setText(user_lat);*/
        }
    }

    protected synchronized void buildGoogleApiClient()
    {
        mGoogleApiClient = new GoogleApiClient.Builder(YourLocationActivity.this)
                .addConnectionCallbacks(YourLocationActivity.this)
                .addOnConnectionFailedListener(YourLocationActivity.this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onPause()
    {
        super.onPause();
        if (mGoogleApiClient != null) {
            try {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, (LocationListener) this);
            } catch (Exception e) {
                // Toast.makeText(getActivity(), "Try Again", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
        mapFragment.onResume();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle)
    {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(10000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(YourLocationActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location)
    {
        try
        {
            mLastLocation = location;
            current_location_latitude = location.getLatitude();
            current_location_longitude = location.getLongitude();
            latLng_origin = new LatLng(location.getLatitude(), location.getLongitude());
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng_origin, 11));
            if(mCurrLocationMarker != null)
            {
                //mCurrLocationMarker.remove();
            }
            else
            {
                MarkerOptions markerOption1 = new MarkerOptions();
                markerOption1.position(latLng_origin);
                markerOption1.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                mCurrLocationMarker = mMap.addMarker(markerOption1);
            }
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());
            addresses = geocoder.getFromLocation(current_location_latitude, current_location_longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = addresses.get(0).getAddressLine(0);
            txtaddress.setText(address);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}